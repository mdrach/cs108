<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Collections"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="database.DBConnection, users.User, achievement.Achievement"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Account Info</title>
</head>
<body>
<%@include file="../include_elements/header.jsp" %>
<%@include file="../include_elements/redirect.jsp" %>
<% 
ServletContext context = getServletContext();
DBConnection db_con = (DBConnection) context.getAttribute("con");
String viewer = (String) request.getSession().getAttribute("user");
String username = request.getParameter("username");
if (username == null) username = viewer;
User user = db_con.getUserByUsername(username);
if(!db_con.canViewProfile(viewer, username)){
	request.getSession().setAttribute("accounterror", "The privacy Settings of " + username +" prevent you from viewing their account page.");
	response.sendRedirect("/cs108/homepage.jsp");		
}
%>

<div class="buttonrow">
<% if (db_con.isAdmin(viewer)){ %>
	<form action="/cs108/AdministrationServlet" method="POST">
		<input type="hidden" list='users' name="DeleteUser" value=<%= username %> > <br>
	<input type=submit value="Delete User">
	</form>
<%} %>

<%
if(!username.equals(viewer)){
	if(!user.isFriends(username)){;
		if(db_con.friendRequestPending(viewer,username)){ 
	     	String acceptServlet = "/cs108/AcceptFriendRequestServlet?requester="+username;%>
			<form action=<%=acceptServlet%> method="post">
			<p><input type = "submit" class = "button" value = "Accept Friend Request" /></p>
			</form>
			<%String rejectServlet = "/cs108/DeleteFriendRequestServlet?requester="+username;%>
			<form action=<%=rejectServlet%> method="post">
			<p><input type = "submit" class = "button" value = "Delete Friend Request" /></p>
			</form>
			
		<%}else if(db_con.friendRequestPending(username,viewer)){%>
			<div class = "button" value = "Friend Request Pending" readonly  /></div>
		<%}else{
			String servletName = "/cs108/SendFriendRequestServlet?requested="+username;%>
			<form action=<%=servletName%> method="post">
			<p><input type = "submit" class = "button" value = "Send Friend Request" /></p>
			</form>
		<%}	
	}
	else{
		String servletName = "/cs108/UnfriendServlet?unfriended="+username+"&unfriender="+viewer;%>
		<form action=<%=servletName%> method="post">
		<p><input type = "submit" value = "Unfriend" /></p>
		</form>
	<%}
	boolean allowedToSendMessageTo = db_con.canMessageEachother(username,viewer);
	if(allowedToSendMessageTo){
		String conversationUrl = "/cs108/user/conversation.jsp";%>
		<form action=<%=conversationUrl%>>
		<input type = "hidden" name = "fromUser" value = "<%=username%>" />
		<p><input type = "submit" value = "Send Message" /></p>
		</form>
	<%}
} else {%>
<a href="conversations.jsp" class="button"> Messages  </a>
<% } %>
</div>

<h1> <%= username %></h1>
<div class = "side-by-side">
<img src = '<%= user.getProfilePictureURL() %>' ALT = 'Profile Picture not found' id='profilepic' width="200" height="200" />

<%if (viewer.equals(username)) { %>

<div id = "recentActivity">
<h2>Friend recent activity</h2>
<% 
	ArrayList<String> recentActivity = db_con.getFriendsRecentHistory(user.getFriends(), 15); 
	for(int i = 0; i < recentActivity.size(); i++) {
		out.print("<li>" + recentActivity.get(i) + "</li>");
		out.print("<br></br>");
	}
%>
<%} %>
</div>
</div>

<h2> Biography </h2>
<p> <%= user.getBiography() %> </p>



<%= user.getAchievementsHTML() %>


<div class = "content">
<h2>Recent Quizzes Taken</h2>
Total Number of Quizzes Taken: <b><%=user.getQuizLogs().size() %></b>
<%
	out.println(shared.TableHelper.generateQuizHistoryTableHTML(db_con, 10, user.getQuizLogs()));
%>
</div>


<% if(user.getQuizzesCreated().size() > 0) { %>
	<div class="content">
	<h2> Quizzes Created </h2>
	Number of quizzes created: <b><%=user.getQuizzesCreated().size() %></b>
	<%= shared.TableHelper.generateQuizTableHTML(user.getQuizzesCreated(), db_con, true) %>
	</div>
<% }%>


<%if (viewer.equals(username)) { %>

<div class = "content">
<h2>Friends</h2>
Total Number of Friends: <b><%= user.getFriends().size() %></b>
<%
	ArrayList<String> friends = new ArrayList<String>(user.getFriends());
	java.util.Collections.sort(friends);
	out.println(shared.TableHelper.generateUserTableHTML(friends));
%>

</div>

<%} %>


<%@include file="../include_elements/footer.jsp"%>

</body>
</html>
