<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@page import="database.DBConnection, users.Message, java.util.List, users.Conversation, java.util.Collections"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@include file="../include_elements/header.jsp" %>
<%@include file="../include_elements/redirect.jsp" %>

<%
String quizCreate = (String) request.getAttribute("message");
if (quizCreate != null){
	out.print("<div class='announcement2'>");
	out.print(quizCreate);
	out.print("</div>");
}
%>

<%
String toUsername = (String)request.getSession().getAttribute("user");
String fromUsername = request.getParameter("fromUser"); 
ServletContext context = getServletContext();
DBConnection db_con = (DBConnection) context.getAttribute("con"); 

String error = "";
if(!db_con.userExists(fromUsername))error = "User does not exist";
else if(toUsername.equals(fromUsername))error = "You can't send messages to yourself.";
else if(!db_con.canMessageEachother(fromUsername,toUsername)){
	int fromPrivacyMessagingSetting = db_con.getPrivacyMessagingSettingByUsername(fromUsername);
	int toPrivacyMessagingSetting = db_con.getPrivacyMessagingSettingByUsername(toUsername);
	if(fromPrivacyMessagingSetting < toPrivacyMessagingSetting && fromPrivacyMessagingSetting!=0)
	  	error = "Your privacy settings won't allow you to message "+fromUsername+".";
	else if(toPrivacyMessagingSetting < fromPrivacyMessagingSetting && toPrivacyMessagingSetting!=0)
	  	error = "The privacy settings of " + fromUsername + " prevent you from messaging them.";
	else 
		error = "Both of your privacy settings prevent you from messaging eachother";
	
}
if(error!=""){
	String conversationsPageRedirectURL = "conversations.jsp?error="+error;
	RequestDispatcher dispatch = request.getRequestDispatcher(conversationsPageRedirectURL);
	dispatch.forward(request,response);
}
else{
	request.getSession().setAttribute("conversationUser",fromUsername);
}

//db_con.readAllMessages(toUsername, fromUsername);
//Conversation conversation = db_con.getConversation(toUsername ,fromUsername);%>
<title>Conversation with <%=fromUsername%></title>
</head>

<form action="conversations.jsp">
<p><input type = "submit" value = "Message Center" /></p>
</form>

<h2> Conversation with 
<%String userAccountPageURL = "/cs108/user/account.jsp?username="+fromUsername;%>
<a href=<%=userAccountPageURL%>> <%=fromUsername%> </a>
</h2>
<body>
<script src="../conversationScript.js"></script>

<div id="conversation"> </div>


<form action="${pageContext.request.contextPath}/SendMessageServlet" method="post">
<p>Message: <input type = "text" name = "messageBody" size=<%=Message.messageWidth%> /></p>
<input type = "hidden" name = "recipient" value = <%=fromUsername%> ></input>
<p><input type = "submit" value = "Send Message" /></p>
</form>
<%@include file="../include_elements/footer.jsp"%>

</body>

</html>