<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page import="database.DBConnection, users.Message, java.util.List, users.ConversationInfo, java.util.Collection, java.util.ArrayList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Message Center</title>
</head>
<body>
<%@include file="../include_elements/header.jsp" %>
<%@include file="../include_elements/redirect.jsp" %>
<%
ServletContext context = getServletContext();
DBConnection db_con = (DBConnection) context.getAttribute("con"); 
String toUser = (String)request.getSession().getAttribute("user");
Collection<ConversationInfo> conversations = db_con.getConversationInfoForUser(toUser, true); 
%>
<%
String quizCreate = (String) request.getAttribute("message");
if (quizCreate != null){
	out.print("<div class='announcement2'>");
	out.print(quizCreate);
	out.print("</div>");
}
%>
<h1> Message Center </h1>
<%//Add table headers %>
<TABLE BORDER="3" CELLSPACING="1" CELLPADDING="1">
<TR>
<TH Align = "center"> Username</TH>
<TH Align = "center"> Last Message Time Stamp</TH>
<TH Align = "center"> Num Unread Messages</TH>
</TR>
<%
	for(ConversationInfo c: conversations){
		if(db_con.canMessageEachother(toUser,c.getFromUsername())){
	%>
	<TR>
	<%//Change get Last Message Field Stub %>
	<% String conversationJsp = "/cs108/user/conversation.jsp?fromUser=" + c.getFromUsername();%>
	<TD ALIGN = "center"> <a href=<%=conversationJsp%>><%=c.getFromUsername()%> </a></TD>
	<TD ALIGN = "center"> <%=c.getTimeLastMessageSent() %> </TD>
	<TD ALIGN = "center"> <%=db_con.numUnreadMessages(toUser,c.getFromUsername()) %> </TD>
	</TR>
	<%}
	}%>
</TABLE>

<% 
//datalist for user delete or promote
int USER_SEARCH_LIMIT = 1000;
ArrayList<String> users = (ArrayList<String>) db_con.searchUsers(USER_SEARCH_LIMIT, "");
out.println("<datalist id='users'>");
for (String u : users){
	out.print("<option value='");
	out.print(u);
	out.print("'>");
}
 out.println("</datalist>");
 %>
 

<form action="conversation.jsp">
<p>User Name: <input type = "text"  list='users' name = "fromUser" size=20 /></p>
<p><input type = "submit" value = "Start New Conversation" /></p>
</form>

<% if(request.getParameter("error")!=null){
	out.println("<div class='announcement2'>");
	out.print(request.getParameter("error"));
	out.print("</div>");
}%>
 


<%@include file="../include_elements/footer.jsp"%>

</body>
</html>