<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Collections"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="database.DBConnection, users.User, achievement.Achievement"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Manage Account</title>
</head>
<body>
<%@include file="../include_elements/header.jsp" %>
<%@include file="../include_elements/redirect.jsp" %>


<%
ServletContext context = getServletContext();
DBConnection db_con = (DBConnection) context.getAttribute("con");
String username = (String)request.getSession().getAttribute("user");
String message = (String) request.getAttribute("message");
%>


<%
if (message != null){
	out.print("<div class='announcement'>");
	out.print(message);
	out.print("</div>");
}
%>


<div class="centered padded">
<h1> Manage Account </h1>
<h2>Change Picture</h2>
<form id="change-picture"  action="/cs108/AccountManagementServlet" method="POST">
	New Picture URL: <input type=text name="picture" required> <br><br>
<input type=submit value="Update Picture">
</form>


<h2>Change Biography</h2>
<form id="change-biography"  action="/cs108/AccountManagementServlet" method="POST">
	Update Biography: <input type=text name="biography" required> <br><br>
<input type=submit value="Update Biography">
</form>

<h2>Change Password</h2>
<form id="change-password"  action="/cs108/AccountManagementServlet" method="POST">
	Old Password:   <input type=password name="oldpassword" type="password" required> <br>
	New Password:   <input type=password name="newpassword" type="password" required>	<br><br>
<input type=submit value="Change Password Account">
</form>

<%int messagingPrivacySetting = db_con.getPrivacyMessagingSettingByUsername(username); %>

<h2>Change Messaging Privacy Settings</h2><h4>(Send/Recieve Messages From)</h4>
<form id="change-privacy-settings"  action="/cs108/AccountManagementServlet" method="POST">
	<input type="radio" name="messagingprivacy" value= 0 <%if(messagingPrivacySetting==0){%> checked="checked"<%}%>>
	Open<br>
	<input type="radio" name="messagingprivacy" value= 1 <%if(messagingPrivacySetting==1){%> checked="checked"<%}%>>
	Friends<br>
	<input type="radio" name="messagingprivacy" value= 2 <%if(messagingPrivacySetting==2){%> checked="checked"<%}%>>
	Friends of Friends<br>
	<input type="radio" name="messagingprivacy" value= 3 <%if(messagingPrivacySetting==3){%> checked="checked"<%}%>>
	Friends of Friends of Friends<br>
	<input type="radio" name="messagingprivacy" value= 4 <%if(messagingPrivacySetting==4){%> checked="checked"<%}%>>
	Friends of Friends of Friends of Friends<br><br>
<input type=submit value="Change Privacy Settings">
</form>


<%int friendRequestPrivacySetting = db_con.getPrivacyFriendRequestSettingByUsername(username); %>
<h2>Change Account Privacy Settings</h2><h4>(Allow to view your Profile/Receive Friend Requests From)</h4>

<form id="change-privacy-settings"  action="/cs108/AccountManagementServlet" method="POST">
	<input type="radio" name="friendrequestprivacy" value= 0 <%if(friendRequestPrivacySetting==0){%> checked="checked"<%}%>>
	Open<br>
	<input type="radio" name="friendrequestprivacy" value= 1 <%if(friendRequestPrivacySetting==1){%> checked="checked"<%}%>>
	Friends<br>
	<input type="radio" name="friendrequestprivacy" value= 2 <%if(friendRequestPrivacySetting==2){%> checked="checked"<%}%>>
	Friends of Friends<br>
	<input type="radio" name="friendrequestprivacy" value= 3 <%if(friendRequestPrivacySetting==3){%> checked="checked"<%}%>>
	Friends of Friends of Friends<br>
	<input type="radio" name="friendrequestprivacy" value= 4 <%if(friendRequestPrivacySetting==4){%> checked="checked"<%}%>>
	Friends of Friends of Friends of Friends<br><br>
<input type=submit value="Change Privacy Settings">
</form>

</div>



<%@include file="../include_elements/footer.jsp"%>

</body>
</html>
