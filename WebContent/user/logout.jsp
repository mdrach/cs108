<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Collections"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="database.DBConnection, users.User, achievement.Achievement"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/cs108/style/style.css"	rel="stylesheet" type="text/css">

<title>Log Out</title>
</head>
<body>
<%@include file="../include_elements/redirect.jsp" %>
<%request.getSession().setAttribute("user",null);%>

<div class="centered pretty-border">
<h1> You have successfully logged out</h1>
<br>
<a href="/cs108/user/login.jsp" class="button centered">Login</a>
</div>
</body>
</html>
