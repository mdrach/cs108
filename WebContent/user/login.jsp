<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="/cs108/style/style.css"	rel="stylesheet" type="text/css">
<title>Please Log in!</title>
</head>
<body>


<div class="centered pretty-border">
<h1>Welcome to Sparkle!  Please log in!</h1>

<form action="/cs108/LoginServlet" method="post" id=login>
User Name: <input type=text name="name"> <br>
<br>
Password: <input type=password name="password" type="password">
<br><br>
<input type=submit value="Login">
</form>
<br>
<form action="/cs108/create_account.jsp">
<input type=submit value="Create new Account">
</form>

</div>

</body>
</html>