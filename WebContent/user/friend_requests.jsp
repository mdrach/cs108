<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@page import="database.DBConnection, users.Message, java.util.List, users.Conversation, java.util.Collections"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>




<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@include file="../include_elements/header.jsp" %>
<%@include file="../include_elements/redirect.jsp" %>



<%
String toUsername = (String)request.getSession().getAttribute("user");
ServletContext context = getServletContext();
DBConnection db_con = (DBConnection) context.getAttribute("con"); 
%>
<title>Friend Requests</title>
</head>
<body>

<h1> Friend Requests </h1>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="../friendRequestsScript.js"></script>

<div id="friendrequests"> </div>

<%@include file="../include_elements/footer.jsp"%>

</body>

</html>