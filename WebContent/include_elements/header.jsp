<link href="${pageContext.request.contextPath}/style/font.css"
	rel="stylesheet" type="text/css">
<link
	href="${pageContext.request.contextPath}/style/header_footer.css"
	rel="stylesheet" type="text/css">
<%@page import="java.util.ArrayList, database.DBConnection, java.util.Random"%>

<div id="header">

<div id="logo"><img
	src="http://www.clker.com/cliparts/K/y/l/A/D/Q/blue-sparkle-md.png">
</div>

<h1><a href="/cs108/homepage.jsp">Sparkle!</a></h1>

<%String quotes [] = {"Home of Many Quizzes", "Where Passion and Quizzes Join", "Where Love Meets A Fun Family Friendly Enviroment", 
		"Your Favorite Quiz Destination", "Home of the Serene", "Have a Great Day!", "Grab a Snack, You'll be Here for a While", "You Look Beautiful Today",
		"Thanks For Visiting!", "A Refreshingly Unique Quiz Website", "Life and Quizzes Are Beautiful", "Keep Calm, be Serene and Take Quizzes",
		"We're Passionate About Quizzes And Love", "Sparkle Together to Light Up the Sky", "Ahead of the Beauty Curve", "Even Better Than Runescape", "The Pleasantries of Life",
		"Amorphous Thoughts and Feelings", "The Duality of Passion and HeartBreak", "A Place of Wonder"};
Random rn = new Random();
int randomIndex = rn.nextInt(quotes.length);
String homePageQuote = quotes[randomIndex];		
%>

<h2><i><%=homePageQuote%></i></h2>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
 
<script>
$(document).ready(function () {
    var interval = 1000;   //number of mili seconds between each call
    var refresh = function() {
        $.ajax({
            url: "/cs108/numUnreadMessagesServlet",
            cache: false,
            success: function(html) {
                $('#numUnreadMessages').html(html);
                setTimeout(function() {
                    refresh();
                }, interval);
            }
        });
    };
    refresh();
});
</script> 

<div id="header-right">
<% if (((DBConnection) getServletContext().getAttribute("con")).isAdmin((String) request.getSession().getAttribute("user"))){ %>
	<a href="/cs108/administration.jsp" id="adminButton">Administration</a>
<%} %>
<!--   <div class="spacer"></div> -->

<form action="/cs108/search_results.jsp" method="post" id="search">
<input name="searchText" type="text" placeholder="Search for users or quizzes..." required> 
<input	type="submit" value="Search">
</form>

</div>

<%String userHeader = (String)request.getSession().getAttribute("user"); %>

<ul>
	<li class="dropdown"><a
		href="/cs108/homepage.jsp">Categories</a>
	<ul>
		<%
			DBConnection headerDBCon = (DBConnection) getServletContext().getAttribute("con");
			ArrayList<Integer> categories= headerDBCon.getCategoryIDs();
			for (int category : categories) {
				out.println("<a href=\"/cs108/homepage.jsp?category=" + category+ "\"><li>"+ 
						headerDBCon.getCategoryFromId(category) + "</li></a>");
			}
			%>
	</ul>
	</li>
	<%String accountJSPURL = "/cs108/user/account.jsp?user=" + request.getSession().getAttribute("user");  %>


	<li><a href="/cs108/RandomQuizServlet">Random Quiz</a></li>
	<li><a href=<%= accountJSPURL %>>My
	Account</a></li>

	<li><a href="/cs108/user/manage_account.jsp"> Manage Account </a></li>
		<li><a href="/cs108/create_quiz/create.jsp">Create
	Quiz</a></li>
<!--		<li><a href="/cs108/homepage.jsp">Leaderboards</a></li>-->
	<li><a href="/cs108/user/logout.jsp"> Logout </a></li>
	

	

	
	

</ul>

<% if(userHeader!=null){ %>
	<div id="numUnreadMessages">
	</div>
<%}%>
</div>



<link href="/cs108/style/style.css"	rel="stylesheet" type="text/css">
<%request.getSession().setAttribute("conversationUser",null); %>
<%if(request.getSession().getAttribute("achievement")!=null){
	int numNewAchievements = (Integer) request.getSession().getAttribute("achievement");
	%>            
	<div class = "announcement2">
	  Achievement Earned! You have earned <%= numNewAchievements %> new achievements!
	</div>
	<% 
	request.getSession().setAttribute("achievement",null);
}
%>


