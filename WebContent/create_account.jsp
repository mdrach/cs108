<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="/cs108/style/style.css"	rel="stylesheet" type="text/css">

<title>Create New Account</title>
</head>
<body>

<div class="centered pretty-border">


<h1>Create New Account</h1>
<p>Please enter proposed name and password: </p>
<form action="/cs108/CreateAccountServlet" method="post">
	User Name: <input type="text" name="name" required> <br>
	<br>
	Password:   <input type="password" name="password" required>
<br><br>
<input type=submit value="Create Account">
</form>

</div>

</body>
</html>