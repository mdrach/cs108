
<%@page import="database.DBConnection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.ArrayList, quiz.QuizInfo"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sparkle</title>
</head>
<body>

<%@include file="include_elements/header.jsp"%>
<%@include file="include_elements/redirect.jsp" %>

<% 
// TODO: Add everything in quizinfo to table
DBConnection db_con = (DBConnection) getServletContext().getAttribute("con");
String categoryString = request.getParameter("category");
int category = categoryString == null ? 0 : Integer.parseInt(categoryString);
%>

<%
String announcement = db_con.getAnnouncement();
if (announcement != null){
	out.println("<div class='announcement'>");
	out.println(announcement);
	out.println("</div>");
}
%>

<%
String quizCreate = (String) request.getSession().getAttribute("quizCreate");
if (quizCreate != null){
	out.print("<div class='announcement2'>");
	out.print(quizCreate);
	out.print("</div>");
	request.getSession().setAttribute("quizCreate", null);
}
%>

<% 
String accounterror = (String)request.getSession().getAttribute("accounterror");
if(accounterror != null){
	out.print("<div class='announcement2'>");
	out.print(accounterror);
	out.print("</div>");
	request.getSession().setAttribute("accounterror", null);
}
%>



<h1> Welcome <%if(request.getParameter("first")==null)out.print(" Back "); %> 
<%= request.getSession().getAttribute("user") %>! </h1>
<%
	if (category != 0) {
		out.print("<h1> Welcome to the " + db_con.getCategoryFromId(category) + " quiz page! </h1>");
	}
%>




<div class="content">
<h2>Most Popular Quizzes</h2>
</div>
<%
	// Popular Quizzes Table
	ArrayList<QuizInfo> popularInfo = db_con.getMostPopularQuizzes(20, category);
	out.print(shared.TableHelper.generateQuizTableHTML(popularInfo, db_con, category == 0));
%>
<div class="content">
<h2>Most Recent Quizzes </h2>
<% 
	// Recently Created Quizzes Table
	ArrayList<QuizInfo> recentInfo = db_con.getMostRecentQuizzes(20, category);
	out.print(shared.TableHelper.generateQuizTableHTML(recentInfo, db_con, category == 0));
%>
</div>


<%@include file="../include_elements/footer.jsp"%>
</body>
</html>