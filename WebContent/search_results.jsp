<%@page import="users.User"%>
<%@page import="quiz.QuizInfo"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Search Results</title>
</head>
<body>

<%@include file="include_elements/header.jsp"%>
<%@include file="../include_elements/redirect.jsp" %>
<% 

ServletContext context = getServletContext();
DBConnection db_con = (DBConnection) context.getAttribute("con");
String searchText = request.getParameter("searchText");
if (searchText == null) {
	RequestDispatcher dispatch = request.getRequestDispatcher("homepage.jsp");
	dispatch.forward(request, response);
}
%>
<h1> Search Results</h1>
<h2>Quizzes</h2>

<%
	// Popular Quizzes Table
	ArrayList<QuizInfo> quizInfo = db_con.searchQuizzes(20, searchText);
	if (quizInfo.size() != 0){
		out.print(shared.TableHelper.generateQuizTableHTML(quizInfo, db_con, true));
	}
%>
<div class = "content">
<h2>Users</h2>
<%
	ArrayList<String> usernames = db_con.searchUsers(20, searchText);
	if (usernames.size() != 0) {
		out.print(shared.TableHelper.generateUserTableHTML(usernames));
	}
	
	if (quizInfo.size() == 0 && usernames.size() == 0) {
		out.print("<h2> No results found. </h2>");
	}
%>
</div>

<%@include file="../include_elements/footer.jsp"%>

</body>
</html>