
$(document).ready(function () {
    var interval = 1000;   //number of mili seconds between each call
    var refresh = function() {
        $.ajax({
            url: "/cs108/ConversationServlet",
            cache: false,
            success: function(html) {
                $('#conversation').html(html);
                setTimeout(function() {
                	refresh();
                }, interval);
            }
        });
    };
    refresh();
});
