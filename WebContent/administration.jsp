<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page import="java.util.ArrayList, quiz.QuizInfo, users.User"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> Administration </title>
</head>
<body>
<%@include file="include_elements/redirect.jsp" %>
<%@include file="include_elements/header.jsp"%>

<%
DBConnection db_con = (DBConnection) getServletContext().getAttribute("con");
String user = (String) request.getSession().getAttribute("user");

String message = (String) request.getAttribute("message");
String userSearch = (String) request.getAttribute("userSearch");

%>

<%
if (!db_con.isAdmin(user)){
	out.print("you must be an administrator to view this page.");
	return;
}
%>

<%

if (message != null){
	
	out.println("<div class='announcement'>");
	out.print(message);
	out.println("</div>");

}
%>

<div class="centered">

<h2>Create Announcement</h2>
<form action="/cs108/AdministrationServlet" method="POST">
	Announcement: <input type=text name="Announcement" required> <br>
<input type=submit value="Post Annoucement">
</form>

<% 
//datalist for user delete or promote
int USER_SEARCH_LIMIT = 1000;
String searchQuery = (userSearch != null) ? userSearch : "";
ArrayList<String> users = (ArrayList<String>) db_con.searchUsers(USER_SEARCH_LIMIT, searchQuery);
out.println("<datalist id='users'>");
for (String u : users){
	out.print("<option value='");
	out.print(u);
	out.print("'>");
}
 out.println("</datalist>");
 %>


<h2>User Account Management</h2>
<form action="/cs108/AdministrationServlet" method="POST">
	Search for user: <input type=text list='users' name="SearchUser" required> <br>
<input type=submit value="Search">
</form>

<%
if (userSearch != null){
	ArrayList<String> usersFound = (ArrayList<String>) db_con.searchUsers(USER_SEARCH_LIMIT, userSearch);
	if (usersFound.size() == 0){
		out.println("No user entries found for search string: " + userSearch);
	}
	else{
		out.print("<h2> Users Found </h2>");
		out.print(shared.TableHelper.generateUserAdministrationHTML(db_con, usersFound));
	}

} else {
	out.println("Please search for a valid user in the box above.");
}
%>
</div>
<%@include file="../include_elements/footer.jsp"%>
</body>
</html>