
$(document).ready(function () {
    var interval = 1000;   //number of mili seconds between each call
    var refresh = function() {
        $.ajax({
            url: "/cs108/FriendRequestsFeedServlet",
            cache: false,
            success: function(html) {
                $('#friendrequests').html(html);
                setTimeout(function() {
                	refresh();
                }, interval);
            }
        });
    };
    refresh();
});
