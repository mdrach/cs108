<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="quiz.Quiz, quiz.Question, java.util.ArrayList, quiz.QuizLog, users.User" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Create a Quiz!</title>
</head>
<body>
<%@include file="../include_elements/header.jsp"%>
<%@include file="../include_elements/redirect.jsp" %>

<h1> Create A Quiz! </h1>

	<div class="content-hi">
		<div id="questions">
			<form class="quiz-creator" action="createQuiz.jsp">
				<div id="errors"></div>
				<!-- General Info about the quiz !-->
				<h2 class="inline"> Quiz category: </h2>
				<select id="category-select">
					<option value="1"> Sports</option>
					<option value="2"> History</option>
					<option value="3"> Music</option>
					<option value="4"> Geography</option>
					<option value="5"> Literature</option>
					<option value="6"> Language</option>
					<option value="7"> Science</option>
					<option value="8"> Television</option>
					<option value="9"> Random </option>
				</select>
				<br>
				<h2 class="inline"> Quiz name: </h2><input type="text" id="quiz-name">
				<br>

				<h2 class="inline"> Quiz Instructions: </h2>
				<textarea id="quiz-instructions" rows="6" cols="50"></textarea>
				<br>

				<h2 class="inline"> Quiz Description: </h2>
				<textarea id="quiz-description" rows="1" cols="50" limit=50></textarea>
				<br>

				<h2 class="inline">Layout: </h2>
				<input type="radio" name="multi" id="multipage-on" value="true">
				<label class="multi"> One question per page</label>
				<input type="radio" name="multi" id="multipage-off" value="false">
				<label class="multi"> All questions on one page</label>
				<br>

				<div id="immediate-feedback">
					<label class="feedback">Immediate feedback: </label>
					<input type="radio" name="feedback" value="true">
					<label class="feedback"> On </label>
					<input type="radio" name="feedback" value="false" checked="checked">
					<label class="feedback"> Off </label>
					<br>
				</div>

				<h2 class="inline">Order: </h2>
				<input type="radio" name="question-order" value="random">
				<label class="order"> random</label>
				<input type="radio" name="question-order" value="fixed">
				<label class="order"> fixed</label>
				<br>
				<hr>
				<br>
				<!-- Add another question to the DB -->
				<form id="add-question">
					<button id="add-question" type="button">Add Question</button>
				</form>
				<button id="create-quiz-button" type='submit'> Create Quiz! </button>
			</form>
		</div>
	</body>

	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>

	<script>
	$("#create-quiz-button").click(function(e){
			// e.preventDefault();
			//make sure everything is filled out
			if (!generalInfoFilled()){
				return;
			} else {
				$("#errors").html("Everything filled out.");

				//vars with general info about the quiz
				var name = $("#quiz-name").val();
				var instructions = $("#quiz-instructions").val();
				var description = $("#quiz-description").val();
				var category = $("#category-select").val();
				var multi = $("input[name='multi']:checked").val();
				console.log("Multi: "+multi);
				var immediate = $("input[name='feedback']:checked").val();
				console.log("Immediate: "+immediate);
				var practice = $("input[name='practice']:checked").val();
				console.log("Practice: "+practice);
				var random = false;
				if ($("input[name='question-order']:checked").val() == "random"){
					random = true;
				}
				console.log(random);
				var questionArr = new Array();
				var counter = 0;
				$(".question-creator").each( function(){
					// console.log($(this).serialize());
					var obj = getQuestionObj(this);
					console.log(obj);
					questionArr.push(getQuestionObj(this));
					counter++;
				});

				console.log("Question Arr: "+questionArr);


				var quizObj = {
					name: name,
					category: category,
					description: description,
					instructions: instructions,
					totalQuestions: counter,
					multi: multi,
					immediate: immediate,
					pracitce: practice,
					random: random,
					questions: questionArr
				};

				postQuiz(quizObj);
				console.log(quizObj);
				window.location.replace("/cs108/homepage.jsp");
			}
		});

	function postQuiz(quiz){
		var successFunc = function (res){
			//do nothing
		};
		
		$.ajax({
			type: "POST",
			url: "/cs108/QuizCreateServlet",
			data: {data: JSON.stringify(quiz)},
			success: successFunc
		});
	}

	//parses a textbox that has different acceptible answers on different lines
	function parseAnswerTextbox(elem){
		return ansArr = $(elem).val().split('\n');
	}

	//returns an object that contains the relevant question info
	function getQuestionObj(elem){
		var type = $(elem).find(".question-type").val();
		console.log("Type: "+type);
		if (type=="multiple-choice"){
			return multipleChoiceObj(elem);
		}

		if  (type=="question-response"){
			return questionResponseObj(elem);
		}

		if (type=="fill-in-the-blank"){
			return fillInTheBlankObj(elem);
		}

		if (type=="picture-response"){
			return pictureResponseObj(elem);
		}
	}

	//creates a multiple-choice question data-object
	function multipleChoiceObj(elem){
		var text = $(elem).find(".question-text").val();
		var answerTextArr = new Array();
		var answer = $(elem).find(".correctAnswerBox:checked").val();

		console.log("Answer: "+answer);
		$(elem).find(".answer-text").each(function(){
			answerTextArr.push($(this).val());
		});
		var multiObj = {
			type: "multiple-choice",
			text: text,
			answerTextArr: answerTextArr,
			answer: answer
		};
		console.log("Multi Object: "+multiObj);
		return multiObj;
	}

	//Creates a fill-in-the-blank data object
	function fillInTheBlankObj(elem){
		var text = "";

		$(elem).find(".question-text").each(function() {
			console.log($(this).val());
			text+= $(this).val();
			text+= " @&@ ";
		});

		text+= $(elem).find(".optional").val();

		// var answerArr = new Array();
		// $(elem).find(".answer-text").each(function(){
		// 	var temp = parseAnswerTextbox(this);
		// 	answerArr.push(temp);
		// });

		var fitbObj = {
			type: "fill-in-the-blank",
			text: text,
			answer: parseAnswerTextbox($(elem).find(".answer-text"))
		};

		// var fitbObj = {
		// 	type: "fill-in-the-blank",
		// 	text: text,
		// 	answers: answerArr
		// };


		return fitbObj;
	}

	function questionResponseObj(elem){
		var text = $(elem).find(".question-text").val();
		var answer = parseAnswerTextbox($(elem).find(".answer-text"));
		var qrObj = {
			type: "question-response",
			text: text,
			answertext: answer
		}

		return qrObj;
	}

	function pictureResponseObj(elem){
		var text = $(elem).find(".question-text").val();
		var answer = parseAnswerTextbox($(elem).find(".answer-text"));
		var url = $(elem).find(".url-text").val();
		var prObj = {
			type: "picture-response",
			text: text,
			answertext: answer,
			url: url
		}
		return prObj;
	}

	function generalInfoFilled() {
		var returnVal = true;
		$(".red").removeClass("red");

		//make sure that the quiz-wide variables have been filled out.
		if ($("#quiz-name").val() == ""){
			$("#quiz-name").addClass("red");
			alert("Yo dawg, this quiz needs a name!");
			returnVal = false;
		}

		if ($("input[name='multi']:checked").length != 1){
			$(".multi").addClass("red");
			alert("Is it one question per page? Or all the questions on one page?");
			returnVal = false;
		} else if ($("#multipage-on").is(":checked")){
			if ($("input[name='feedback']:checked").length != 1){
				$(".feedback").addClass("red");
				alert("Do you want immediate feedback? Or feedback just at the end?");
				returnVal = false;
			}
		}
		console.log("Category: "+$("#category-select").val());
		if ($("#category-select").val() ==""){
			$("#category-select").addClass("red");
			alert("You need to select a category.");
			returnVal = false;
		}

		if ($("input[name='question-order']:checked").length != 1){
			$(".order").addClass("red");
			alert("Indicate whether you want a fixed or random order!");
			returnVal = false;
		}

		if ($("#quiz-instructions").val() == ""){
			$("#quiz-instructions").addClass("red");
			alert("Fill out the quiz instructions");
			returnVal = false;
		}

		if ($("#quiz-description").val() == ""){
			$("#quiz-description").addClass("red");
			alert("Fill out the quiz description");
			returnVal = false;
		}


		//makesure that the qustion fields have all been filled out.
		$.each($(".question-text"), function(){
			if ($(this).val() == ""){
				$(this).addClass("red");
				alert("You actually need to write the question dude...");
				returnVal = false;
			}
		});
		//check all the answer fields
		$.each($(".answer-text"), function(){
			if ($(this).val() == ""){
				$(this).addClass("red");
				alert("You actually need to write answers dude... We don't play WallStreet style");
				returnVal = false;
			}
		});
		//make sure they include a URL in pic questions
		$.each($(".url-text"), function(){
			if ($(this).val() == ""){
				$(this).addClass("red");
				alert("You actually need to include a url brosky.");
				returnVal = false;
			}
		});

		//should check that the answer table has 1 correct answer
		//but I think it's unnecessary b/c I init it with 1 selected
		$.each($(".answerTable"), function(){
			var correctAnswer = false;
			if($(this).find(".correctAnswerBox:checked").length != 1){
				$(this).addClass("red");
				alert("You actually need to include a right answer brosky.");
				returnVal = false;
			}
		});

		return returnVal;
	}
	</script>
	<script><!--
	$(document).ready(function(){
		console.log("Javascript Connected.");
		var questionHeader = "";
		$("#immediate-feedback").hide();
			//Init Listeners
			$("#multipage-on").click(function(){
				console.log("selected");
				$("#immediate-feedback").fadeIn();
			});

			$("#multipage-off").click(function(){
				console.log("selected");
				$("#immediate-feedback").fadeOut();
			});

			//Set event listener on add question button
			addQuestionListener();
			//listen to changes in the question type
			addQuestionTypeListeners();
			addQuestion();

		});
	</script>

	<script><!--
	//Listeners
	//EventListener for selecting question type
	function addQuestionTypeListeners() {
		$(".question-type").change(function(){
			console.log("Question type selected.");
			var value = $(this).find("option:selected").val();
			console.log("value: "+value);
			if (value =="question-response"){
				$(this).parent().find(".question-template").html(questionResponseHtml());
			} else if (value =="multiple-choice"){
				$(this).parent().find(".question-template").html(multipleChoiceHtml());
			} else if (value =="picture-response"){
				$(this).parent().find(".question-template").html(pictureResponseHtml());
			} else if (value =="fill-in-the-blank"){
				$(this).parent().find(".question-template").html(fillInTheBlankHtml());
				// fillInTheBlankListeners();
			}
		});
	}

	function addQuestionListener() {
		$('#add-question').click(function() {
			console.log("Adding question.");
			$('#add-question').before(makeNewQuestion);
			$(".question-type").off();
			addQuestionTypeListeners();
			addDeleteListeners();
		});
	}
	
	function addQuestion(){
		console.log("Adding question.");
		$('#add-question').before(makeNewQuestion);
		$(".question-type").off();
		addQuestionTypeListeners();
		//addDeleteListeners();
		$(".delete").remove();
		
	}

	function addDeleteListeners(){
		$(".delete").off();
		$(".delete").click(function(){
			$(this).parent().remove();
		});
	}
	
	//Event Listeners for fill in the blank questions
	function fillInTheBlankListeners(){
		deleteBlankListeners();
		//make sure not to duplicate
		$(".number-blanks").off();
		//add # blank listner
		$(".number-blanks").change(function() {
			//get the table elem
			var table = $(this).parent().parent().parent();
			//remove the existing rows
			table.find(".question-row").remove();
			var numberBlanks = $(this).val();
			for (var i = 1; i < numberBlanks; i++){
				table.find(".last-row").before(fillInTheBlankRowHtml());
			}
			deleteBlankListeners();
		}); 
	}

	function deleteBlankListeners(){
		//remove existing listeners
		$(".delete-blank").off();
		//add delete listener
		$(".delete-blank").click(function (e){
			e.preventDefault();
			$(this).parent().parent().remove();
			$(".number-blanks").val($(".number-blanks").val() - 1);
		});
	}
	--></script>
	<script>
			//Function that writes the general HTML for making a question header
			function makeQuestionHeader(questionNumber){
				var html = "<form class='question-creator'>";
				// html+= "<input type='hidden' name='questionNumber' value="+questionNumber+ ">";
				html+= "<label> Question type: </label>";
				html+= "<select name='question-type' class='question-type'>";
				html+= "<option value='multiple-choice'> Multiple-Choice </option>";
				html+= "<option value='question-response'> Question Response</option>";
				html+= "<option value='fill-in-the-blank'> Fill-in-the-blank</option>";
				html+= "<option value='picture-response'> Picture Response </option>";
				html+= "</select>";
				html+= "<button type='button' class='delete-q'>Delete Question</button>";
				html+= "<br>";
				html+= "<div class='question-template'>";
				html+= "</div> <br><hr><br></form>";
				return html;
			}

			//Function that writes the HTML for new question, initialized as multiple choice
			function makeNewQuestion(questionNumber){
				var html = "<form class='question-creator'>";
				html+= "<label> Question type: </label>";
				html+= "<select name='question-type' class='question-type'>";
				html+= "<option value='multiple-choice'> Multiple-Choice </option>";
				html+= "<option value='question-response'> Question Response</option>";
				html+= "<option value='fill-in-the-blank'> Fill-in-the-blank</option>";
				html+= "<option value='picture-response'> Picture Response </option>";
				html+= "</select>";
				html+= "<button type='button' class='delete'>Delete question</button>";
				html+= "<br>";
				html+= "<div class='question-template'>";
				html+= multipleChoiceHtml();
				html+= "</div><br><hr><br></form>";
				return html;

			}
			</script>
			<script>

	//generates HTML for the question response form
	function questionResponseHtml(){
		// var html = "<input type='hidden' name='questionType' value='question-response'>";
		var html= "<label> Question Text </label>";
		html+= "<br>";
		html +=	"<br>";
		html+= "<textarea class='question-text' rows='6' cols='50'></textarea>";
		html+= "<br>";
		html+= "<label> Answer Text </label>";
		html+= "<br>";
		html+= "<p> Optional: To add multiple acceptible answers, seperate each answer in the textbox with newline.</p>";
		html+= "<br>";
		html+= "<textarea class='answer-text' rows='4' cols='45'></textarea>";

		return html;
	}

	//generates HTML for the multiple-choice form
	function multipleChoiceHtml(){
		// var html = "<input type='hidden' name='questionType' value='multiple-choice'>";
		var html =	"<label> Question Text </label>";
		html +=	"<br>";
		html += "<textarea class='question-text' rows='6' cols='50'></textarea>";
		html += "<br>";
		html += "<label> Answers </label>";
		html += "<br>";
		html += "<table class='answerTable'>";
		html +=	"<tr class='table-header'>";
		html += "<th> Correct </th>";
		html +=	"<th> Answer Text</th>";
		html += "</tr>";
		html += "<tr class='possibleAnswer'>";
		html += "<td class='possibleAnswerCheck'>";
		html += "<input type='radio' class='correctAnswerBox' name='correct' value='1' checked='checked'>";
		html += "</td>";
		html +=	"<td class='possibleAnswerText'>";
		html += "<input type='text' name='answer1text' class='answer-text'>";
		html += "</td>";
		html += "</tr>";
		html += "<tr class='possibleAnswer'>";
		html += "<td class='possibleAnswerCheck'>";
		html += "<input type='radio' class='correctAnswerBox' name='correct' value='2'>";
		html += "</td>";
		html += "<td class='possibleAnswerText'>";
		html += "<input type='text' name='answer2text' class='answer-text'>";
		html += "</td>";
		html += "</tr>";
		html += "<tr class='possibleAnswer'>";
		html += "<td class='possibleAnswerCheck'>";
		html += "<input type='radio' class='correctAnswerBox' name='correct' value='3'>";
		html += "</td>";
		html += "<td class='possibleAnswerText'>";
		html += "<input type='text' name='answer3text' class='answer-text'>";
		html += "</td>";
		html += "</tr>";
		html += "<tr class='possibleAnswer'>";
		html += "<td class='possibleAnswerCheck'>";
		html += "<input type='radio' class='correctAnswerBox' name='correct' value='4'>";
		html += "</td>";
		html += "<td class='possibleAnswerText'>";
		html += "<input type='text' name='answer4text' class='answer-text'>";
		html += "</td>";
		html += "</tr>";
		html += "</table>";
		html += "<br>";
		return html;
	}

	function pictureResponseHtml() {
		// var html = "<input type='hidden' name='questionType' value='picture-response'>";
		var html = "<label> Picture URL: </label>";
		html += "<input type='text' class='url-text'>";
		html += "<br>";
		html += "<label> Answer Text </label>";
		html += "<br>";
		html += "<p> Optional: To add multiple acceptible answers, seperate each answer in the textbox with newline.</p>";
		html += "<br>";
		html += "<p> To accept multiple answers, put each possibility on a different line. </p>";
		html += "<textarea class='answer-text' rows='4' cols='45'></textarea>";
		return html;
	}

	function fillInTheBlankHtml() {
		// var html = "<input type='hidden' name='questionType' value='fill-in-the-blank'>";
		var html = "<p> Optional: To add multiple acceptible answers, seperate each answer in the textbox with newline.</p>";
		html +="<br>";
		html +="<table class='fill-in-the-blank-table'>";
		html += "<tr class='table-header'>";
		html += "<th> Question Text </th>";
		html += "<th> Blank</th>";
		html += "</tr>";
		html += "<tr class='permanent-question-row'>";
		html += "<td class='question-text-cell'>";
		html += "Before Blank <textarea class='question-text' rows='3'></textarea>";
		html += "</td>";
		html += "<td class='possibleAnswerText'>";
		html += "<textarea class='answer-text' rows='3'></textarea>";
		html += "</td>";
		html += "</tr>";
		html += "<tr class='last-row'>";
		html += "<td class='question-final-cell'>";
		html += "After Blank <textarea class='optional' rows='3'></textarea>";
		html += "</td>";
		html += "</tr>";
		html += "</table>";
		html += "<br>";
		return html;
	}

	function fillInTheBlankRowHtml() {
		var html = "<tr class='question-row'>";
		html += "<td class='question-text-cell'>";
		html += "<textarea class='question-text' rows='3'></textarea>";
		html += "</td>";
		html += "<td class='possibleAnswerText'>";
		html += "<textarea class='answer-text' rows='3'></textarea>";
		html += "<button class='delete-blank' type='button' url=''>Delete Row</button>";
		html += "</td>";
		html += "</tr>";

		return html;
	}
	</script>
	
	
<%@include file="../include_elements/footer.jsp"%>

</body>
</html>