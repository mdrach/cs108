<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="quiz.Quiz, quiz.QuizLog, quiz.Question, java.util.ArrayList, java.util.Set, users.User, quiz.ChallengeQuizInfo"%>

    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Results</title>
</head>
<body>
<%@include file="../include_elements/header.jsp"%>
<%@include file="../include_elements/redirect.jsp"%>

<% 
ServletContext context = getServletContext();
DBConnection db_con = (DBConnection) context.getAttribute("con");
QuizLog quizLog = (QuizLog) request.getSession().getAttribute("quizLog");
Quiz quiz = (Quiz) request.getSession().getAttribute("quiz");
User user = db_con.getUserByUsername((String)request.getSession().getAttribute("user"));
boolean challengeQuiz = (request.getParameter("challengeRequest")!=null);
ChallengeQuizInfo info = (ChallengeQuizInfo) request.getSession().getAttribute("challengerInfo");
%>
<h1>
<% out.print(quiz.getName()+ " ");%>
Complete! :-D </h1>
<h3 class="right plain"> Time Taken: 
<% out.print(((double)quizLog.getTimeTakenToComplete())/1000 + " seconds and got "); %>
<br> Questions Correct: 
<% out.print(quizLog.getNumQuestionsCorrect() + " out of " + quiz.getNumQuestions() +  " questions correct"); %>
</h3>
<%
if(challengeQuiz){
	boolean challengerWon = false;
	if(info.getNumQuestionsCorrect() > quizLog.getNumQuestionsCorrect()){
		out.print("<b>"+info.getChallenger()+" beat you!</b>");
		challengerWon = true;
	}
	else{
		if(info.getNumQuestionsCorrect() == quizLog.getNumQuestionsCorrect()){
			if(info.getTimeTaken() > quizLog.getTimeTakenToComplete()){
				out.print("<h3><b>You Beat"+ info.getChallenger()+"</b></h3>");
			}
			else{
				out.print("<h3><b>"+info.getChallenger()+" beat you!</b></h3>");
				challengerWon = true;
			}
		}
		else{
			out.print("<b>You Beat"+ info.getChallenger()+"</b>");
		}
	}
	db_con.sendChallengeResponse(info.getChallenger(), (String)request.getSession().getAttribute("user"), challengerWon, quiz.getId());
}
%> 

<%
int i = 0;
out.print("<div class='quiz'>");
ArrayList<String> userResponses = quizLog.getUserResponses();
Set<Integer> correctQuestions = quizLog.getWhichQuestionsCorrect();

for (Question q : quiz.getQuestions()){
	boolean correct = correctQuestions.contains(q.getId());

	out.print("<div class='question'>");
	out.print("<h3>Question " + Integer.toString(i+1) + "</h3>");
	if(challengeQuiz){
		if(info.challengerGotQuestionCorrect(q.getId())){
			out.println("<h3>"+info.getChallenger()+" got it Correct</h3>" );
		}
		else{
			out.println("<h3>"+info.getChallenger()+" got it Incorrect</h3>" );
		}
	}
	out.print(q.getCorrectedHTML(userResponses.get(i), correct));
	out.print("</div'>");
	i++;
}
out.print("<div class='quiz'>");
%>

<% 
//datalist for user delete or promote
int USER_SEARCH_LIMIT = 1000;
ArrayList<String> users = new ArrayList<String>(user.getFriends());
out.println("<datalist id='users'>");
for (String u : users){
	out.print("<option value='");
	out.print(u);
	out.print("'>");
}
 out.println("</datalist>");
 %>

<%if(user.getFriends().size()!=0){%>
<br/><br/>
<form action="/cs108/ChallengeUserToQuizServlet" method="POST" class="text-align-right">
<h3 class="inline"> Challenge A friend to Take the Quiz! </h3> 
<input type=text list='users' name="challenged" required> <br>
<input type=hidden name="id" value = <%=quiz.getId()%>>
<input type=submit value="Challenge Friend!">
</form>
<br/>
<%}%>

<a href="/cs108/homepage.jsp" class="button right">Take another quiz</a>


<%@include file="../include_elements/footer.jsp"%>




</body>
</html>