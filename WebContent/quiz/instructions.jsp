<%@page import="users.User"%>
<%@page import="quiz.QuizLog"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="quiz.Quiz, quiz.Question, java.util.ArrayList" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Instructions</title>
</head>
<body>
<%@include file="../include_elements/header.jsp"%>
<%@include file="../include_elements/redirect.jsp" %>

<%
Quiz quiz = (Quiz) request.getSession().getAttribute("quiz");
DBConnection db_con = (DBConnection) request.getServletContext().getAttribute("con");
User viewer = db_con.getUserByUsername((String)request.getSession().getAttribute("user"));

%>

<div class="quiz">
<h1> <%= quiz.getName() %> </h1>
<div class="quiz-description">
<%=quiz.getDescription() %> 
<br></br>
Created by <%= quiz.getCreator() %>
</div>
<% if (viewer.isAdmin()){ %>
	<form action="/cs108/AdministrationServlet" method="POST">
		<input type="hidden" list='users' name="DeleteQuiz" value=<%=quiz.getId() %> required> <br>
	<input type=submit value="Delete Quiz">
	</form>
<%} %>

<div class="quiz-instructions"> 
<h4>Instructions:</h4>
<%=quiz.getInstructions()%>
</div>

<div class='content'>
<h2> Top Scorers </h2>
<%
	if(viewer.hasPlayedQuiz(quiz.getId())) {
		out.print("Your top score: <b>" + viewer.getBestScore(quiz.getId()) + "</b>");
	}

	ArrayList<QuizLog> topPerformances = db_con.getTopScorers(10, quiz.getId());
	out.print(shared.TableHelper.generateTopScorersTableHTML(topPerformances));
%>
</div>

<br></br>

<form action="QuizServlet" method="POST">
	<input class="submit-button" type="submit" value="Start!" />
</form>

</div>


<%@include file="../include_elements/footer.jsp"%>


</body>
</html>
