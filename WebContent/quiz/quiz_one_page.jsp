<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="quiz.Quiz, quiz.Question, java.util.ArrayList, quiz.QuizLog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<% Quiz quiz = (Quiz) request.getSession().getAttribute("quiz"); %>
<title><%=quiz.getName()%></title>
</head>
<body>
<%@include file="../include_elements/header.jsp"%>
<%@include file="../include_elements/redirect.jsp" %>

<form action="SinglePageSubmitServlet" method="POST" class="quiz">
<%
int i = 1;
for (Question q : quiz.getQuestions()){
	out.print("<div class='question'>");
	
	out.print("<h3>Question " + i + "</h3>");
	out.print(q.getHTML());
	
	out.print("</div>");
	i++;
}
if(request.getParameter("challengeRequest")!=null){%>
<input type = "hidden" name = "challengeRequest" value= <%=request.getParameter("challengeRequest")%>/>
<%}%>
<input class="submit-button" type="submit" value="Submit Answers" />
</form>

<%@include file="../include_elements/footer.jsp"%>
</body>
</html>