<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="quiz.Quiz, quiz.Question, java.util.ArrayList, quiz.QuizLog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%
Quiz quiz = (Quiz) request.getSession().getAttribute("quiz");
int questionNum = (Integer) request.getSession().getAttribute("currentQuestion");
%>
<title> <%=quiz.getName() %></title>
</head>
<body>
<%@include file="../include_elements/header.jsp"%>
<%@include file="../include_elements/redirect.jsp" %>

<form action="/cs108/MultiPageSubmitServlet" method="POST" class="quiz">
<%
Question q = quiz.getQuestions().get(questionNum - 1);  // Subtract 1 because ArrayLists are 0-indexed.

/* Print question. */
out.print("<div class='question'>");
out.print("<h3>Question " + questionNum + "</h3>");
out.print(q.getHTML());
out.print("</div>");

/* Print question number. */
out.print("<p>Question " + questionNum + " of " + quiz.getNumQuestions() + "</p>");

/* Add button. */
out.print("<input class='submit-button' type='submit' value='Next' />");
if(request.getParameter("challengeRequest")!=null){%>
<input type = "hidden" name = "challengeRequest" value= <%=request.getParameter("challengeRequest")%>/>
<%}%>
</form>
<%@include file="../include_elements/footer.jsp"%>
</body>
</html>