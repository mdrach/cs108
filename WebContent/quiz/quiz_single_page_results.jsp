<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page
	import="quiz.Quiz,quiz.QuizLog,quiz.Question,java.util.ArrayList,java.util.Set"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Question Result</title>
</head>
<body>
<%@include file="../include_elements/header.jsp"%>
<%@include file="../include_elements/redirect.jsp"%>

<% 
Quiz quiz = (Quiz) request.getSession().getAttribute("quiz");
QuizLog quizLog = (QuizLog) request.getSession().getAttribute("quizLog");
int nextQuestion = ((Integer) request.getSession().getAttribute("currentQuestion"));
%>

<%
String userResponse = quizLog.getMostRecentUserResponse();
Set<Integer> correctQuestions = quizLog.getWhichQuestionsCorrect();
Question q = quiz.getQuestion(nextQuestion - 2);  // -1 to get last question, -1 to get index of array (-2)
boolean correct = correctQuestions.contains(q.getId());
if(!correct)
	out.print("<h3>Incorrect</h3>");
else
	out.print("<h3>Correct</h3>");
out.print(q.getCorrectedHTML(userResponse, correct));

%>

<% 
/* Go to next question if quiz not over. */
if (nextQuestion <= quiz.getNumQuestions()){
	out.print("<form action='/cs108/quiz/quiz_multi_page.jsp' method='POST' class='quiz'>");
	if(request.getParameter("challengeRequest")!=null){
		out.print("<input type = 'hidden' name = 'challengeRequest' value= "+request.getParameter("challengeRequest")+">");
	}
	out.print("<input class='submit-button' type='submit' value='Next Question' />");
}
/* Go to results page if quiz over. */
else {
	out.print("<form action='/cs108/quiz/quiz_results.jsp' method='POST'>");
	if(request.getParameter("challengeRequest")!=null){
		out.print("<input type = 'hidden' name = 'challengeRequest' value= "+request.getParameter("challengeRequest")+">");
	}
	out.print("<input class='submit-button' type='submit' value='See Results' />");
}
out.print("</form>");
%>

<%@include file="../include_elements/footer.jsp"%>


</body>
</html>