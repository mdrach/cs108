<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="database.DBConnection, quiz.ChallengeQuizInfo, quiz.Quiz"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Challenge Quiz Results</title>
</head>
<body>
<%@include file="../include_elements/header.jsp"%>
<%@include file="../include_elements/redirect.jsp" %>
<% 
	DBConnection db_con = (DBConnection) getServletContext().getAttribute("con"); 
	String currentUser = (String)request.getSession().getAttribute("user");
	String challengeQuizIDString = request.getParameter("id");
	
	int challengeQuizID = Integer.parseInt(challengeQuizIDString);
	String challenger = request.getParameter("challenger");
	ChallengeQuizInfo info = db_con.getChallengeQuizInfo(challengeQuizID, challenger);
	//TODO: GET THIS WORKING
	info.setChallenger(challenger);
	Quiz quiz = db_con.getQuizById(Integer.parseInt(challengeQuizIDString));
	request.getSession().setAttribute("quiz", quiz);
	request.getSession().setAttribute("challengerInfo", info);
%>
<h1> <%= info.getChallenger()%>  challenged you to take quiz "<%= db_con.getQuizNameById(challengeQuizID) %>"! </h1>
<div class = "centered text-center">
<%= info.getHTMLCentered() %>
<br>
<form action="/cs108/QuizServlet" method="POST" >
	<input type="hidden" name="challengeRequest" value="true" >
	<input class="submit-button" type="submit" value="Try to beat <%= info.getChallenger() %>!" />
</form>
</div>

</body>
</html>