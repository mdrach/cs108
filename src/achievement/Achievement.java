package achievement;

public class Achievement {
	private int id;
	private String achievementName;
	private String description;
	private int pointValue;
	private String timeReceived;
	private String username;
	
	public static final int AMATEUR_AUTHOR = 0;
	public static final int PROLIFIC_AUTHOR = 1;
	public static final int PRODIGIOUS_AUTHOR = 2;
	public static final int QUIZ_MACHINE = 3;
	public static final int QUIZ_MANIAC = 4;
	public static final int QUIZ_HOPPER = 5;
	public static final int JACK_OF_TRADES = 6;
	public static final int THE_GREATEST = 7;
	public static final int PERFECTION = 8;
	public static final int SPEED_DEMON = 9;
	
	public static final Achievement[] achievements = {
		new Achievement(AMATEUR_AUTHOR, "Amateur Author", "Create a quiz", 5, "",""),
		new Achievement(PROLIFIC_AUTHOR, "Prolific Author", "Create 5 quizzes", 10, "",""),
		new Achievement(PRODIGIOUS_AUTHOR, "Prodigious Author", "Create 10 quizzes", 20, "", ""),
		new Achievement(QUIZ_MACHINE, "Quiz Machine", "Take 10 quizzes", 5, "", ""),
		new Achievement(QUIZ_MANIAC, "Quiz Maniac", "Take 25 quizzes", 10, "", ""),
		new Achievement(QUIZ_HOPPER, "Quiz Hopper", "Take 5 different quizzes", 5, "", ""),
		new Achievement(JACK_OF_TRADES, "Jack of All Trades", "Take a quiz in 5 different categories", 10, "",""),
		new Achievement(THE_GREATEST, "I am the Greatest", "Get the Highest Score on a Quiz", 20, "", ""),
		new Achievement(PERFECTION, "Perfection", "Get a perfect score on a Quiz", 10, "", ""),
		new Achievement(SPEED_DEMON, "Speed Demon", "Finish a quiz with a perfect score in under 30 seconds", 20, "","")
	};
	
	Achievement(int id, String achievementName, String description , int pointValue, String username, String timeReceived){
		this.id = id;
		this.achievementName = achievementName;
		this.description = description;
		this.pointValue = pointValue;
		this.username = username;
		this.timeReceived = timeReceived;
	}
	
	public static Achievement getAchievementByID(int achievementID, String username, String timeStamp){
		Achievement result = achievements[achievementID];
		result.timeReceived = timeStamp;
		result.username = username;
		return result;
	}
	
	public int getId() {
		return id;
	}

	public int getPointValue() {
		return pointValue;
	}
	
	public String getAchievementName(){
		return achievementName;
	}
	
	public String getDescription(){
		return this.description;
	}

	public String getTimeReceived(){
		return this.timeReceived;
	}
	
	public String getUsername() {
		return username;
	}
}


