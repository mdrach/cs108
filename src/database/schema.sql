USE c_cs108_mdrach;

DROP TABLE IF EXISTS quizlog;
DROP TABLE IF EXISTS questions;
DROP TABLE IF EXISTS achievements;
DROP TABLE IF EXISTS history;
DROP TABLE IF EXISTS messages;
DROP TABLE IF EXISTS friends;
DROP TABLE IF EXISTS administration;
DROP TABLE IF EXISTS quizzes;
DROP TABLE IF EXISTS users;

SET @@auto_increment_increment=1;

CREATE TABLE quizzes (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100),
    description VARCHAR(500),
    instructions VARCHAR(500),
    creator VARCHAR(100),
    creationtime TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    timesplayed INT DEFAULT 0,
    randomquestion BOOLEAN,
    onlyonepage BOOLEAN,
    immediatecorrection BOOLEAN,
    category INT
);

CREATE TABLE questions (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    quiz INT,
    prompt VARCHAR(500),
    type INT,
    choices VARCHAR(500) DEFAULT NULL,
    correctchoice INT,
    answer VARCHAR(500)
);

CREATE TABLE users (
    username VARCHAR(100) PRIMARY KEY,
    passwordhash VARCHAR(256),
    biography VARCHAR(2000) DEFAULT 'This user has not yet created a biography.',
    profile_picture VARCHAR(500) DEFAULT 'http://cops.usdoj.gov/html/dispatch/01-2013/images/Blankman.jpg',
    privacy_setting INT DEFAULT 0,
    creationtime TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    administrator BOOLEAN DEFAULT FALSE
);

CREATE TABLE achievements (
    username VARCHAR(100),
    achievement INT,
    timereceived TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE quizlog (
    username VARCHAR(100),
    quiz INT,
    numquestions INT,
    numquestionscorrect INT,
    whichquestioncorrect VARCHAR(500),
    timetaken BIGINT,
    timefinished TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE messages (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    to_username VARCHAR(100),
    from_username VARCHAR(100),
    message_type INT,
    message_seen BOOLEAN,
    message_field VARCHAR(2000),
    challenge_quiz_id INT,
    time_sent DATETIME
);

CREATE TABLE friends (
    user1 VARCHAR(100),
    user2 VARCHAR(100),
    friendship_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE administration (
    announcement VARCHAR(100) DEFAULT NULL
);


INSERT INTO users (username, passwordhash, administrator) VALUES
	("user1", "5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8", true);

-- 	enum- SPORTS, HISTORY, MUSIC, GEOGRAPHY, LITERATURE, LANGUAGE, SCIENCE, TELEVISION
INSERT INTO quizzes VALUES
	(1,"Quiz1","Our first quiz", "Answer these questions","user1", CURRENT_TIMESTAMP, 0, false, true, false, 9),
    (2, "Quiz2","Multiple page", "Answer these questions","user1", CURRENT_TIMESTAMP, 0, false, false, true, 9);

INSERT INTO questions VALUES
	(1, 1, "What is the answer to life the universe and everything?", 1, "", 1, "42"),
	(2, 1, "The color of the sky is @&@, like the sea.", 2, "", 0, "blue@&@clear"),
	(3, 1, "Who was the first president?", 3, "Washington @&@ Lincoln @&@ Monroe @&@ Madison", 1, "Washington"),
	(4, 2, "http://media.npr.org/images/picture-show-flickr-promo.jpg", 4, "", 0, "squirrel"),
	(5, 2, "Is Mitch cool?", 1, "", 0, "yes@&@no"),
	(6, 2, "Astronauts eat @&@ in space", 2, "", 1, "food"),
	(7, 2, "Apples or Oranges?", 1, "text of multiple choices", 3, "apples@&@oranges"),
	(8, 1, "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTldCVPiSHG0Fp6E-96vCu8mXyctKcIA6tmb1J698HCfb5W3urF", 4, "", 0, "lemon");
	
	


INSERT INTO achievements VALUES
	("user1", 0, current_timestamp);

	
INSERT INTO administration VALUES
	("This is an announcement created on the administration page.");
