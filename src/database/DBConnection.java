package database;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import achievement.Achievement;
import quiz.*;
import users.Conversation;
import users.ConversationInfo;
import users.FriendRequests;
import users.User;
import users.Message;


public class DBConnection {

	private static final String DB_INIT_FILE = "schema.sql";
	private Connection db_con;

	// Categories
	private Map<Integer, String> categories;

	public DBConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			db_con = (Connection) DriverManager.getConnection
			( "jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME , MyDBInfo.MYSQL_PASSWORD);
			Statement stmt = db_con.createStatement();
			stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
		} catch (SQLException e) {
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		initCategories();
	}


	public String getCategoryFromId(int categoryId) {
		return categories.get(categoryId);
	}

	public ArrayList<Integer> getCategoryIDs (){
		return new ArrayList<Integer>(categories.keySet());
	}

	/* Returns an ArrayList of QuizInfo objects, used to
	 * populate the list of quizes to take on the user's home page.
	 */
	public ArrayList<QuizInfo> getQuizTitles(int numQuizzes){
		ArrayList<QuizInfo> quizInfo = new ArrayList<QuizInfo>();
		try {
			PreparedStatement query = db_con.prepareStatement("SELECT * FROM quizzes LIMIT ?");
			query.setInt(1, numQuizzes);
			ResultSet results = query.executeQuery();
			while (results.next()){
				QuizInfo info = getQuizInfoFromResultSet(results);
				quizInfo.add(info);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return quizInfo;
	}

	/* Returns an arraylist of quizinfo objects, ordered by quizzes which have been played the most
	 * to populate the list of quizzes to take on the user's home page. If category = 0, then it is 
	 * not ordered by category. Else, there are only quizzes of a certain category.
	 */
	public ArrayList<QuizInfo> getMostPopularQuizzes(int numQuizzes, int category) {
		ArrayList<QuizInfo> quizInfo = new ArrayList<QuizInfo>();
		try {
			PreparedStatement query;
			if (category == 0){
				query = db_con.prepareStatement("SELECT * FROM quizzes ORDER BY timesplayed DESC LIMIT ?");
				query.setInt(1, numQuizzes);
			} else {
				query = db_con.prepareStatement("SELECT * FROM quizzes WHERE category = ? ORDER BY timesplayed DESC LIMIT ?");
				query.setInt(1, category);
				query.setInt(2, category);
			}
			ResultSet results = query.executeQuery();
			while (results.next()){
				QuizInfo info = getQuizInfoFromResultSet(results);
				quizInfo.add(info);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return quizInfo;
	}


	/* Same as above, but lists most recent quizzes 
	 */
	public ArrayList<QuizInfo> getMostRecentQuizzes(int numQuizzes, int category) {
		ArrayList<QuizInfo> quizInfo = new ArrayList<QuizInfo>();
		try {
			PreparedStatement query;
			if (category == 0){
				query = db_con.prepareStatement("SELECT * FROM quizzes ORDER BY creationtime DESC LIMIT ?");
				query.setInt(1, numQuizzes);
			} else {
				query = db_con.prepareStatement("SELECT * FROM quizzes WHERE category = ? ORDER BY creationtime DESC LIMIT ?");
				query.setInt(1, category);
				query.setInt(2, category);
			}
			ResultSet results = query.executeQuery();
			while (results.next()){
				QuizInfo info = getQuizInfoFromResultSet(results);
				quizInfo.add(info);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return quizInfo;
	}

	/* returns a Statement object for running sql queries */
	public Statement getStatement() {
		try {
			return (Statement) db_con.createStatement();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/* Returns ResultSet from Query executed in String form. */
	public Statement getStmt(){
		Statement stmt = null;
		try {
			stmt = db_con.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return stmt;
	}

	/* Returns true if username exists. */
	public boolean userExists(String username) {
		ResultSet results = null;
		PreparedStatement query = null;
		boolean returnval = false;
		try {
			query = db_con.prepareStatement("SELECT * FROM users WHERE username=?");
			query.setString(1, username);
			results = query.executeQuery();
			if (results.next())  
				returnval = true; 
		}
		catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try { if (query != null) query.close(); }
			catch (SQLException e){
				e.printStackTrace();
			}
		}
		return returnval;
	}

	public User getUserByUsername(String username){
		try {
			PreparedStatement query = db_con.prepareStatement("SELECT * FROM users WHERE username = ?");
			query.setString(1, username);
			ResultSet results = query.executeQuery();
			while(results.next()) {
				return getUserFromResultSet(results);
			}
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return null;
	}

	/* Returns true if username and password are valid. */
	// finally block setup from: http://stackoverflow.com/questions/1812891/java-escape-string-to-prevent-sql-injection
	public boolean validLogin(String username, String password) {
		ResultSet results = null;
		boolean returnval = false;
		try {
			PreparedStatement query = db_con.prepareStatement("SELECT * FROM users WHERE username=? AND passwordhash=?");
			query.setString(1, username);
			query.setString(2, ComputeHash(password));
			results = query.executeQuery();

			// If query was empty null the username/password pair was valid.
			if (results.next())  
				returnval = true; 
		}
		catch (SQLException e) {
			e.printStackTrace();
		} 
		return returnval;
	}

	public void createNewAccount (String username, String password){
		try {
			PreparedStatement query = db_con.prepareStatement("INSERT INTO users (username, passwordhash, creationTime) Values (?, ?, current_timestamp)");
			query.setString(1, username);
			query.setString(2, ComputeHash(password));
			query.execute();
		}
		catch (SQLException e) {
			e.printStackTrace();
		} 
	}


	/* Disconnects from the database. */
	public void disconnect(){
		try {
			db_con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/* Returns the string representation of the hashed password. */
	private String ComputeHash(String password) {
		MessageDigest digest = null;
		try {
			digest = MessageDigest.getInstance("SHA");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return hexToString(digest.digest(password.getBytes()));
	}

	/*
	 Given a byte[] array, produces a hex String,
	 such as "234a6f". with 2 chars for each byte in the array.
	 (provided code)
	 */
	private static String hexToString(byte[] bytes) {
		StringBuffer buff = new StringBuffer();
		for (int i=0; i<bytes.length; i++) {
			int val = bytes[i];
			val = val & 0xff;  // remove higher bits, sign
			if (val<16) buff.append('0'); // leading 0
			buff.append(Integer.toString(val, 16));
		}
		return buff.toString();
	}

	public Quiz getQuizById(int quizId) {
		Quiz result = null;
		try {
			PreparedStatement query = db_con.prepareStatement("SELECT * FROM quizzes WHERE id = ?");
			query.setInt(1, quizId);
			ResultSet results = query.executeQuery();
			while (results.next()){
				result = getQuizFromResultSet(results);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public String getQuizNameById(int quizId) {
		try {
			PreparedStatement query = db_con.prepareStatement("SELECT name FROM quizzes WHERE id = ?");
			query.setInt(1, quizId);
			ResultSet results = query.executeQuery();
			if (results.next()){
				return results.getString("name");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}



	public int storeQuizResult (QuizLog quizLog){

		// load user and check for achievements earned
		User u = getUserByUsername(quizLog.getUsername());
		int numAchievementsRecieved=  u.checkForAndAddAchievements(this, quizLog);


		StringBuilder resultString = new StringBuilder();
		for (Integer correctQuestion : quizLog.getWhichQuestionsCorrect()){
			resultString.append(correctQuestion.toString() + Question.DELIMITER);
		}
		try {
			PreparedStatement query = db_con.prepareStatement("INSERT INTO quizlog VALUES (?, ?, ?, ?, ?, ?, current_timestamp)");
			query.setString(1, quizLog.getUsername());
			query.setInt(2, quizLog.getQuizId());
			query.setInt(3, quizLog.getNumQuestions());
			query.setInt(4, quizLog.getNumQuestionsCorrect());
			query.setString(5, resultString.toString());
			query.setLong(6, quizLog.getTimeTakenToComplete());
			query.execute();
		}
		catch (SQLException e) {
			e.printStackTrace();
		} 
		// Update quiz to have one more time played.
		try {
			PreparedStatement query = db_con.prepareStatement("UPDATE quizzes SET timesplayed = timesplayed + 1 WHERE id = ?");
			query.setInt(1, quizLog.getQuizId());
			query.executeUpdate();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return numAchievementsRecieved;
	}

	public void insertAchievement(int achievementId, String username) {
		try {
			PreparedStatement query = db_con.prepareStatement("INSERT INTO achievements (username, achievement) VALUES (?, ?)");
			query.setString(1, username);
			query.setInt(2, achievementId);
			query.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}


	public void sendMessage(String toUsername, String fromUsername, int messageType, String messageField, int challengeQuizID){
		try{
			PreparedStatement query = db_con.prepareStatement("INSERT INTO messages "
					+ "(to_username, from_username, message_type, message_seen, message_field, challenge_quiz_id, time_sent ) "
					+ "VALUES (?, ?, ?, ?, ?, ?, current_timestamp)");
			query.setString(1, toUsername);
			query.setString(2, fromUsername);
			query.setInt(3, messageType);
			query.setBoolean(4, false);
			query.setString(5, messageField);
			query.setInt(6, challengeQuizID);
			query.execute();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}

	public void seeMessage(int messageID){
		try{
			PreparedStatement query = db_con.prepareStatement("UPDATE messages SET message_seen = ? WHERE id = ?");
			query.setBoolean(1,true);
			query.setInt(2, messageID);
			query.execute();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}

	public Message getMessageByID(int id){
		try{
			PreparedStatement query = db_con.prepareStatement("Select * FROM Messages WHERE id = ?");
			query.setInt(1,id);
			ResultSet results = query.executeQuery();
			Message message = null;
			if(results.next()){
				String toUsername = results.getString("to_username");
				String fromUsername = results.getString("from_username");
				int messageType = results.getInt("message_type");
				boolean messageSeen = results.getBoolean("message_seen");
				String messageField = results.getString("message_field");
				int challengeQuizID = results.getInt("challenge_quiz_id");
				String timeSent = results.getTimestamp("time_sent").toString();
				message = new Message(id, toUsername, fromUsername, messageType, messageField, messageSeen, challengeQuizID, timeSent);
			}
			return message;
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return null;
	}

	public List<Message> getMessages(String toUsername){
		try{
			PreparedStatement query = db_con.prepareStatement("SELECT * FROM messages WHERE to_username = ?");
			query.setString(1,toUsername);
			ResultSet results = query.executeQuery();
			List<Message> messages = new ArrayList<Message>();
			while(results.next()){
				Message m = getMessageFromResultSet(results);
				messages.add(m);
			}
			return messages;
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		return null;
	}

	public FriendRequests getFriendRequests(String toUsername) {
		List<Message> messages = new ArrayList<Message>();
		List<Message> toMessages = getMessagesOfType(toUsername, null, false);
		List<Message> fromMessages = getMessagesOfType(null, toUsername, false);		
		if(toMessages!=null)messages.addAll(toMessages);
		if(fromMessages!=null)messages.addAll(fromMessages);
		return new FriendRequests(messages, toUsername);
	}
	
	public Conversation getConversation(String toUsername, String fromUsername){
		List<Message> messages = new ArrayList<Message>();
		List<Message> toMessages = getMessagesOfType(toUsername, fromUsername, true);
		List<Message> fromMessages = getMessagesOfType(fromUsername, toUsername, true);		
		if(toMessages!=null)messages.addAll(toMessages);
		if(fromMessages!=null)messages.addAll(fromMessages);
		return new Conversation(messages, toUsername, fromUsername);
	}

	private PreparedStatement getMessagesOfTypePreparedStatement(String toUsername, String fromUsername, boolean fetchingStandardMessages){
		try{
			String queryString = "SELECT * FROM messages WHERE";
			if(toUsername != null)queryString += " to_username = ? ";
			if(toUsername != null && fromUsername != null)queryString += " AND from_username = ?";
			else if(fromUsername!=null)queryString += " from_username = ?";
			if(fetchingStandardMessages)queryString += " AND (message_type = 1 OR message_type = 2 OR message_type = 3)";
			else queryString += " AND (message_type = 0)";
			PreparedStatement query = db_con.prepareStatement(queryString);
			if(toUsername != null){
				query.setString(1,toUsername);
				if(fromUsername!=null)query.setString(2,fromUsername);
			}
			else {
				if(fromUsername!=null)query.setString(1,fromUsername);
			}
			return query;
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Message> getMessagesOfType(String toUsername, String fromUsername, boolean fetchingStandardMessages){
		try{
			PreparedStatement query = getMessagesOfTypePreparedStatement(toUsername, fromUsername, fetchingStandardMessages);
			ResultSet results = query.executeQuery();
			List<Message> messages = new ArrayList<Message>();
			while(results.next()){
				Message m = getMessageFromResultSet(results);
				messages.add(m);
			}
			return messages;
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Message> getFriendRequestMessages(String toUsername, String fromUsername){
		return null;
	}
	
	private PreparedStatement getFriendRequestsQuery(boolean for_to_user, String username){
		try{
			String queryString = "SELECT * FROM messages WHERE";
			if(for_to_user)queryString += " to_username = ?";
			else queryString += " from_username = ?";
			queryString += " And message_type = ?";
			PreparedStatement query = db_con.prepareStatement(queryString);
			query.setString(1,username);
			query.setInt(2, 0);
			return query;
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return null;
	}
	
	private PreparedStatement getMessagesRequestQuery(boolean for_to_user, String username){
		try{
			String queryString = "SELECT * FROM messages WHERE";
			if(for_to_user)queryString += " to_username = ?";
			else queryString += " from_username = ?";
			queryString += " And (message_type = ? OR message_type = ? OR message_type = ?)";
			PreparedStatement query = db_con.prepareStatement(queryString);
			query.setString(1,username);
			query.setInt(2, 1);
			query.setInt(3, 2);
			query.setInt(4, 3);
			return query;
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return null;
	}
	
	private PreparedStatement getConversationRequestQuery(boolean messages, boolean for_to_user, String username){
		if(messages)return getMessagesRequestQuery(for_to_user, username);
		else return getFriendRequestsQuery(for_to_user, username);
	}

	public Collection<ConversationInfo> getConversationInfoForUser(String toUsername, boolean fetchingMessages){
		try{
			Map<String, ConversationInfo> conversationInfoForUser = new HashMap<String,ConversationInfo>();
			Set<String> usersThatHaveSentMessages = new HashSet<String>();
			PreparedStatement query = getConversationRequestQuery(fetchingMessages, true, toUsername);
			ResultSet results = query.executeQuery();
			while(results.next()){
				String fromUsername = results.getString("from_username");
				String messageField = results.getString("message_field");
				String timeLastMessageSent = results.getString("time_sent");
				if(!usersThatHaveSentMessages.contains(fromUsername)){
					usersThatHaveSentMessages.add(fromUsername);
					conversationInfoForUser.put(fromUsername, new ConversationInfo(timeLastMessageSent , 
							(messageField.length()>ConversationInfo.stubLength) ? messageField.substring(ConversationInfo.stubLength) : messageField,toUsername,fromUsername));
				}
				else {ConversationInfo convInfo = conversationInfoForUser.get(fromUsername);
				if(convInfo.getTimeLastMessageSent().compareTo(timeLastMessageSent) > 0 ){
					convInfo.setTimeLastMessageSent(timeLastMessageSent);
					convInfo.setLastMessageFieldStub(messageField.substring(ConversationInfo.stubLength));
				}
				}
			}
			PreparedStatement query2 = getConversationRequestQuery(fetchingMessages, false, toUsername);
			results = query2.executeQuery();
			while(results.next()){
				String to_username = results.getString("to_username");
				String messageField = results.getString("message_field");
				String timeLastMessageSent = results.getString("time_sent");
				if(!usersThatHaveSentMessages.contains(to_username)){

					conversationInfoForUser.put(to_username, new ConversationInfo(timeLastMessageSent , 
							(messageField.length()>ConversationInfo.stubLength) ? messageField.substring(ConversationInfo.stubLength) : messageField,toUsername,to_username));
				}
			}
			return conversationInfoForUser.values();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return null;
	}

	public ArrayList<QuizInfo> searchQuizzes(int limit, String searchText) {
		ArrayList<QuizInfo> info = new ArrayList<QuizInfo>();
		PreparedStatement query;
		try {
			query = db_con.prepareStatement("SELECT * FROM quizzes WHERE name LIKE ? OR description LIKE ? ORDER BY id DESC LIMIT ? ");
			query.setString(1,  "%" + searchText + "%");
			query.setString(2, "%" + searchText + "%");
			query.setInt(3, limit);
			ResultSet results = query.executeQuery();
			while(results.next()) {
				info.add(getQuizInfoFromResultSet(results));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return info;
	}

	public ArrayList<String> searchUsers(int limit, String searchText) {
		ArrayList<String> users = new ArrayList<String>();
		PreparedStatement query;
		try {
			query = db_con.prepareStatement("SELECT username FROM users WHERE username LIKE ? ORDER BY username ASC LIMIT ? ");
			query.setString(1, "%" + searchText + "%");
			query.setInt(2, limit);
			ResultSet results = query.executeQuery();
			while(results.next()) {
				users.add(results.getString("username"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return users;
	}

	private void initCategories(){
		categories = new HashMap<Integer, String>();
		categories.put(1, "Sports");
		categories.put(2, "History");
		categories.put(3, "Music");
		categories.put(4, "Geography");
		categories.put(5, "Literature");
		categories.put(6, "Language");
		categories.put(7, "Science");
		categories.put(8, "Television");
		categories.put(9, "Random");
	}

	private Quiz getQuizFromResultSet(ResultSet results) {
		int quizId;
		try {
			quizId = results.getInt("id");
			String name = results.getString("name");
			String description = results.getString("description");
			String instructions = results.getString("instructions");
			String creator = results.getString("creator");
			boolean randomizedOrder = results.getBoolean("randomquestion");
			boolean onePage = results.getBoolean("onlyonepage");
			boolean immediateCorrection = results.getBoolean("immediatecorrection");
			Quiz q = new Quiz(quizId, name, description, instructions, creator, randomizedOrder, onePage, immediateCorrection, Integer.parseInt(results.getString("category")));
			addQuestionsToQuiz(q, randomizedOrder);
			return q;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;

	}

	private void addQuestionsToQuiz(Quiz q, boolean randomizedOrder){
		int quizId = q.getId();

		try {
			PreparedStatement query = db_con.prepareStatement("SELECT * FROM questions WHERE quiz = ?");
			query.setInt(1, quizId);
			ResultSet results = query.executeQuery();
			while(results.next()) {
				Question toBeAdded = getQuestionFromResultSet(results);
				q.addQuestion(toBeAdded);
			}
			if (randomizedOrder){
				q.randomizeQuestions();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private Message getMessageFromResultSet(ResultSet results) {
		try {
			int messageID = results.getInt("id");
			String toUsername = results.getString("to_username");
			String fromUsername = results.getString("from_username");
			int messageType = results.getInt("message_type");
			boolean messageSeen = results.getBoolean("message_seen");
			String messageField = results.getString("message_field");
			int challengeQuizID = results.getInt("challenge_quiz_id");
			String timeSent = results.getString("time_sent");
			return new Message(messageID, toUsername, fromUsername, messageType, messageField, messageSeen,
					challengeQuizID, timeSent);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;

	}

	private QuizInfo getQuizInfoFromResultSet(ResultSet results) {
		try {
			QuizInfo info = new QuizInfo();
			info.name = results.getString("name");
			info.description = results.getString("description");
			info.id = results.getString("id");
			info.creator = results.getString("creator");
			info.categoryId = results.getInt("category");
			info.timesPlayed = results.getInt("timesplayed");
			return info;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;	
	}

	private User getUserFromResultSet(ResultSet results) {
		try {
			String username = results.getString("username");
			String hash = results.getString("passwordhash");
			String biography = results.getString("biography");
			String pictureURL = results.getString("profile_picture");
			int privacySetting = results.getInt("privacy_setting");
			String creationTime = results.getString("creationtime");
			boolean admin = results.getBoolean("administrator");
			User u = new User(username, hash, biography, pictureURL, privacySetting, creationTime, admin);
			addAchievementsToUser(u);
			addFriendsToUser(u);
			addQuizLogsToUser(u);
			addQuizzesCreatedToUser(u);
			return u;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void addAchievementsToUser(User u) {
		try {
			PreparedStatement query = db_con.prepareStatement("SELECT * FROM achievements WHERE username = ? ORDER BY timereceived ASC");
			query.setString(1, u.getUsername());
			ResultSet results = query.executeQuery();
			while(results.next()) {
				Achievement a = getAchievementFromResultSet(results);
				u.addAchievement(a);
			}
		} catch(SQLException ignored){}
	}

	private void addFriendsToUser(User u) {
		try {
			PreparedStatement query = db_con.prepareStatement("SELECT * FROM friends WHERE user1 = ? OR user2 = ?");
			query.setString(1, u.getUsername());
			query.setString(2, u.getUsername());
			ResultSet results = query.executeQuery();
			while(results.next()) {
				String user1 = results.getString("user1");
				String user2 = results.getString("user2");
				if (u.getUsername().equals(user1)){
					u.addFriend(user2);
				} else {
					u.addFriend(user1);
				}
			}	
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}

	private Set<String> getFriends(String username){
		try{
			Set<String> friends = new HashSet<String>();
			PreparedStatement query = db_con.prepareStatement("SELECT * FROM friends WHERE user1 = ? OR user2 = ?");
			query.setString(1, username);
			query.setString(2, username);
			ResultSet results = query.executeQuery();
			while(results.next()) {
				String user1 = results.getString("user1");
				String user2 = results.getString("user2");
				if (username.equals(user1)){
					friends.add(user2);
				} else {
					friends.add(user1);
				}
			}	
			return friends;
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void addQuizLogsToUser(User u) {
		try {
			PreparedStatement query = db_con.prepareStatement("SELECT * FROM quizlog WHERE username = ? ORDER BY timefinished ASC");
			query.setString(1, u.getUsername());
			ResultSet results = query.executeQuery();
			while(results.next()) {
				QuizLog log = getQuizLogFromResultSet(results);
				u.addQuizLog(log);
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}

	private void addQuizzesCreatedToUser(User u) {
		try {
			PreparedStatement query = db_con.prepareStatement("SELECT * FROM quizzes WHERE creator = ?");
			query.setString(1, u.getUsername());
			ResultSet results = query.executeQuery();
			while(results.next()) {
				QuizInfo info = getQuizInfoFromResultSet(results);
				u.addQuizCreated(info);
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}

	private QuizLog getQuizLogFromResultSet(ResultSet results) {
		try {
			String username = results.getString("username");
			int quizId = results.getInt("quiz");
			int numQuestions = results.getInt("numquestions");
			int numQuestionsCorrect = results.getInt("numquestionscorrect");
			String[] which = results.getString("whichquestioncorrect").split(Question.DELIMITER);
			Set<Integer> questionsCorrect = new HashSet<Integer>();
			for (int i = 0; i < which.length; i++) {
				if (which[i].length() > 0) {
					questionsCorrect.add(Integer.parseInt(which[i]));
				}
			}
			long timeTaken = results.getLong("timetaken");
			Date timeFinished = results.getDate("timefinished");
			return new QuizLog(username, quizId, numQuestions, numQuestionsCorrect, questionsCorrect, timeTaken, timeFinished.getTime());
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private Achievement getAchievementFromResultSet(ResultSet results) {
		try {
			String username = results.getString("username");
			int id = results.getInt("achievement");
			String date = results.getString("timereceived");
			return Achievement.getAchievementByID(id, username, date);
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private Question getQuestionFromResultSet(ResultSet results) {
		try {
			int questionId = results.getInt("id");
			String prompt = results.getString("prompt");
			int quizId = results.getInt("quiz");
			switch(results.getInt("type")){

			case Question.QUESTION_TYPE_MULTIPLE:
				return new MultipleChoiceQuestion(questionId, quizId, prompt, results.getString("choices"), results.getInt("correctchoice"));
			case Question.QUESTION_TYPE_PICTURE:
				return new PictureResponseQuestion(questionId, quizId, prompt, results.getString("answer"));
			case Question.QUESTION_TYPE_RESPONSE:
				return new ResponseQuestion(questionId, quizId, prompt, results.getString("answer"));
			case Question.QUESTION_TYPE_BLANK:
				return new FillInBlankQuestion(questionId, quizId, prompt, results.getString("answer"));
			}
		} catch(SQLException e) {}
		return null;
	}

	private Set<Integer> parseQuestionsCorrectString(String whichQuestionsCorrectString){
		Set<Integer> questionsAnsweredCorrectly = new HashSet<Integer>();
		String delim = "[" + Question.DELIMITER + "]";
		String[] tokens = whichQuestionsCorrectString.split(delim);
		for(String a: tokens){
			questionsAnsweredCorrectly.add(Integer.parseInt(a));
		}
		return questionsAnsweredCorrectly;
	}


	public void makeFriends(String username1, String username2) {

		try{
			PreparedStatement query = db_con.prepareStatement("INSERT INTO friends (user1, user2, friendship_timestamp) VALUES(?, ?, current_timestamp)");
			query.setString(1,username1);
			query.setString(2,username2);
			query.execute();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}


	public boolean friends(String username1, String username2) {
		try{
			PreparedStatement query = db_con.prepareStatement("SELECT COUNT(*) As count FROM friends WHERE (user1 = ? AND user2 = ?)OR (user1 = ? AND user2 = ?)");
			query.setString(1,username1);
			query.setString(2,username2);
			query.setString(3,username2);
			query.setString(4,username1);
			ResultSet resultSet = query.executeQuery();
			int count = 0;
			if(resultSet.next()) count = resultSet.getInt("count");
			if(count>0)return true;
			else return false;
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return false;
	}

	public boolean friendRequestPending(String toUsername, String fromUsername){
		List<Message> messages = getMessagesOfType(toUsername, null, false);
		for(Message m: messages){
			if(m.getFromUsername().equals(fromUsername))return true;
		}
		return false;
	}

	public int numUnreadMessages(String username){
		try{
			String queryString = "SELECT COUNT(*) As count FROM messages WHERE to_username = ? AND message_seen = ?";
			queryString += " And (message_type = 1 OR message_type = 2 OR message_type = 3)";
			PreparedStatement query = db_con.prepareStatement(queryString);
			query.setString(1,username);
			query.setBoolean(2,false);
			ResultSet resultSet = query.executeQuery();
			resultSet.next();
			int numUnreadMessages = resultSet.getInt("count");
			return numUnreadMessages;
		}catch(SQLException e){
			e.printStackTrace();
		}
		return -1;
	}

	public int numUnseenFriendRequests(String username){
		try{
			String queryString = "SELECT COUNT(*) As count FROM messages WHERE to_username = ? AND message_seen = ?";
			queryString += " And message_type = 0";
			PreparedStatement query = db_con.prepareStatement(queryString);
			query.setString(1,username);
			query.setBoolean(2,false);
			ResultSet resultSet = query.executeQuery();
			resultSet.next();
			int numUnreadMessages = resultSet.getInt("count");
			return numUnreadMessages;
		}catch(SQLException e){
			e.printStackTrace();
		}
		return -1;
	}

	public int getNumQuizzes(){
		try{
			PreparedStatement query = db_con.prepareStatement("SELECT COUNT(*) as count FROM quizzes");
			ResultSet resultSet = query.executeQuery();
			resultSet.next();
			int count = resultSet.getInt("count");
			return count;
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return -1;

	}

	public ArrayList<QuizLog> getTopScorers(int limit, int quizId) {
		ArrayList<QuizLog> topScorers = new ArrayList<QuizLog>();
		Set<String> topUsers = new HashSet<String>();
		try {
			PreparedStatement query = db_con.prepareStatement("SELECT * FROM quizlog WHERE quiz = ? ORDER BY numquestionscorrect DESC, timetaken ASC");
			query.setInt(1, quizId);
			ResultSet results = query.executeQuery();
			while(results.next() && topScorers.size() <= limit) {
				QuizLog log = getQuizLogFromResultSet(results);
				if (!topUsers.contains(log.getUsername())) {
					topScorers.add(log);
					topUsers.add(log.getUsername());
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return topScorers;
	}

	private boolean friendOfNFriendsRecursion(String currentUser, String desiredUser, int currentDegrees,int maxDegrees){
		if(currentUser.equals(desiredUser)) return true;
		if(currentDegrees == maxDegrees)return false;
		Set<String> friends = getFriends(currentUser);
		boolean result = false;
		if(friends!=null){
			for(String user: friends){
				result = (result || friendOfNFriendsRecursion(user ,desiredUser, currentDegrees+1, maxDegrees));
			}
		}
		return result;
	}

	private int getPrivacySettingByUsername(String username){
		try {
			PreparedStatement query = db_con.prepareStatement("SELECT * FROM users WHERE username = ?");
			query.setString(1, username);
			ResultSet results = query.executeQuery();
			if(results.next()) {
				return (results.getInt("privacy_setting"));
			}
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return -1;
	}
	
	public int getPrivacyMessagingSettingByUsername(String username){
		return getPrivacySettingByUsername(username)%10;
	}
	
	public int getPrivacyFriendRequestSettingByUsername(String username){
		return getPrivacySettingByUsername(username)/10;
	}
	
	public boolean canViewProfile(String viewer, String user){
		int userPrivacySetting = getPrivacyFriendRequestSettingByUsername(user);
		if(userPrivacySetting == 0) return true;
		return friendOfNFriendsRecursion(user, viewer, 0, userPrivacySetting);
	}

	public boolean canMessageEachother(String username1, String username2){
		int user1PrivacySetting = getPrivacyMessagingSettingByUsername(username1);
		int user2PrivacySetting = getPrivacyMessagingSettingByUsername(username2);
		if(user1PrivacySetting == 0 && user2PrivacySetting == 0)return true;
		int lowestPrivacySetting = 0;
		if(user1PrivacySetting == 0)lowestPrivacySetting = user2PrivacySetting;
		else if(user2PrivacySetting == 0)lowestPrivacySetting = user1PrivacySetting;
		else if(user1PrivacySetting < user2PrivacySetting)lowestPrivacySetting = user1PrivacySetting;
		else if(user2PrivacySetting <= user1PrivacySetting)lowestPrivacySetting = user2PrivacySetting;
		return friendOfNFriendsRecursion(username1, username2, 0, lowestPrivacySetting);
	}

	public void changePassword(String user, String password){
		String passwordHash = ComputeHash(password);
		try {
			PreparedStatement query = db_con.prepareStatement("UPDATE users SET passwordhash=? WHERE username=?");
			query.setString(1, passwordHash);
			query.setString(2, user);
			query.executeUpdate();
		}
		catch (SQLException e){
			e.printStackTrace();
		}
	}
	public void changePicture(String user, String picture){
		try {
			PreparedStatement query = db_con.prepareStatement("UPDATE users SET profile_picture=? WHERE username=?");
			query.setString(1, picture);
			query.setString(2, user);
			query.executeUpdate();
		}
		catch (SQLException e){
			e.printStackTrace();
		}
	}

	public void changeBiography(String user, String biography){
		try {
			PreparedStatement query = db_con.prepareStatement("UPDATE users SET biography=? WHERE username=?");
			query.setString(1, biography);
			query.setString(2, user);
			query.executeUpdate();
		}
		catch (SQLException e){
			e.printStackTrace();
		}
	}

	public void changeFriendRequestPrivacySetting(String user, int friendRequestPrivacySetting){
		try {
			int previousPrivacySetting = getPrivacySettingByUsername(user);
			PreparedStatement query = db_con.prepareStatement("UPDATE users SET privacy_setting=? WHERE username=?");
			query.setInt(1, (friendRequestPrivacySetting*10 + previousPrivacySetting%10));
			query.setString(2, user);
			query.executeUpdate();
		}
		catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	public void changeMessagingPrivacySetting(String user, int messagingPrivacySetting){
		try {
			int previousPrivacySetting = getPrivacySettingByUsername(user);
			PreparedStatement query = db_con.prepareStatement("UPDATE users SET privacy_setting=? WHERE username=?");
			query.setInt(1, ((previousPrivacySetting/10)*10)+messagingPrivacySetting);
			query.setString(2, user);
			query.executeUpdate();
		}
		catch (SQLException e){
			e.printStackTrace();
		}
	}

	public void deleteFriendShip(String user1, String user2){
		if(friends(user1,user2)){
			try{
				PreparedStatement query = db_con.prepareStatement("DELETE FROM friends WHERE ((user1 = ? AND user2 = ?) OR (user1 = ? AND user2 = ?))");
				query.setString(1,user1);
				query.setString(2,user2);
				query.setString(3,user2);
				query.setString(4,user1);
				query.execute();
			}
			catch(SQLException e){
				e.printStackTrace();
			}
		}
	}

	public void deleteFriendRequest(String requested, String friendRequester) {
		try{
			PreparedStatement query = db_con.prepareStatement("DELETE FROM messages WHERE (to_username = ? AND from_username= ? AND message_type = ?)");
			query.setString(1,requested);
			query.setString(2,friendRequester);
			query.setInt(3, 0);
			query.execute();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}

	public void setAnnouncement (String announcement){
		try {
			PreparedStatement query = db_con.prepareStatement("UPDATE administration SET announcement=?");
			query.setString(1, announcement);
			query.executeUpdate();
		}
		catch (SQLException e){
			e.printStackTrace();
		}
	}

	public String getAnnouncement (){
		try {
			PreparedStatement query = db_con.prepareStatement("SELECT announcement FROM administration LIMIT 1");
			ResultSet results = query.executeQuery();
			if(results.next()) {
				return results.getString("announcement");
			}
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		return null;
	}

	public boolean isAdmin(String user){
		try {
			PreparedStatement query = db_con.prepareStatement("SELECT administrator FROM users where username=?");
			query.setString(1, user);
			ResultSet results = query.executeQuery();
			if(results.next()) {
				return results.getBoolean("administrator");
			} else {
				return false;
			}
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		return false;

	}

	public void promoteUser(String user){
		try {
			PreparedStatement query = db_con.prepareStatement("UPDATE users SET administrator=TRUE WHERE username=?");
			query.setString(1, user);
			query.executeUpdate();
		}
		catch (SQLException e){
			e.printStackTrace();
		}
	}

	public void demoteUser(String user){
		try {
			PreparedStatement query = db_con.prepareStatement("UPDATE users SET administrator=FALSE WHERE username=?");
			query.setString(1, user);
			query.executeUpdate();

		}
		catch (SQLException e){
			e.printStackTrace();
		}
	}

	public void deleteQuizByID (int id){
		try {
			PreparedStatement deleteQuestions = db_con.prepareStatement("DELETE FROM questions WHERE quiz=?");
			deleteQuestions.setInt(1, id);
			PreparedStatement deleteQuiz = db_con.prepareStatement("DELETE FROM quizzes WHERE id=?");
			deleteQuiz.setInt(1, id);
			deleteQuestions.execute();
			deleteQuiz.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void deleteUserQuizzes (String user){
		ArrayList<Integer> toDelete = new ArrayList<Integer>();
		try {
			PreparedStatement getQuizzes = db_con.prepareStatement("SELECT id FROM quizzes WHERE creator=?");
			getQuizzes.setString(1, user);
			ResultSet rs = getQuizzes.executeQuery();
			while(rs.next()){
				toDelete.add(rs.getInt("id"));
			}
			for (int i : toDelete){
				deleteQuizByID (i);
				PreparedStatement deleteCreatedQuizLog = db_con.prepareStatement("DELETE FROM quizlog WHERE quiz=?");
				deleteCreatedQuizLog.setInt(1, i);
				deleteCreatedQuizLog.execute();
			}
		}
		catch (SQLException e){
			e.printStackTrace();
		}
	}

	public void deleteUser (String user){
		try {
			PreparedStatement deleteUser = db_con.prepareStatement("DELETE FROM users WHERE username=?");
			deleteUser.setString(1, user);

			PreparedStatement deleteQuizlog = db_con.prepareStatement("DELETE FROM quizlog WHERE username=?");
			deleteQuizlog.setString(1, user);


			PreparedStatement deleteMessages = db_con.prepareStatement("DELETE FROM messages WHERE to_username=? OR from_username=?");
			deleteMessages.setString(1, user);
			deleteMessages.setString(2, user);

			PreparedStatement deleteFriendships = db_con.prepareStatement("DELETE FROM friends WHERE user1=? OR user2=?");
			deleteFriendships.setString(1, user);
			deleteFriendships.setString(2, user);
			
			PreparedStatement deleteAchievements = db_con.prepareStatement("DELETE FROM achievements WHERE username=?");
			deleteAchievements.setString(1, user);
			
			deleteAchievements.execute();
			deleteQuizlog.execute();
			deleteMessages.execute();
			deleteFriendships.execute();
			deleteUser.execute();

			deleteUserQuizzes (user);
		}
		catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	public void readAllFriendRequests(String viewer){
		try{
			String queryString = "UPDATE messages SET message_seen=TRUE WHERE to_username= ?";
			queryString+=" AND message_type = 0";
			PreparedStatement query = db_con.prepareStatement(queryString);
			query.setString(1,viewer);
			query.execute();
			
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public void readAllMessages(String viewer, String fromUser){
		try{
			String queryString = "UPDATE messages SET message_seen=? WHERE to_username=? AND from_username=?";
			queryString += (" And (message_type = 1 OR message_type = 2 OR message_type = 3)");
			PreparedStatement query = db_con.prepareStatement(queryString);
			query.setBoolean(1,true);
			query.setString(2,viewer);
			query.setString(3,fromUser);
			query.execute();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}

	public ChallengeQuizInfo getChallengeQuizInfo(int challengeQuizID, String challenger){
		try{
			PreparedStatement query = db_con.prepareStatement("Select * from quizlog Where quiz = ? AND username = ?");
			query.setInt(1,challengeQuizID);
			query.setString(2,challenger);
			ResultSet results = query.executeQuery();
			ChallengeQuizInfo info = null;
			int mostQuestionsCorrect = -1;
			long bestTimeTaken = -1;
			while(results.next()){
				int numQuestions = results.getInt("numquestions");
				int numQuestionsCorrect = results.getInt("numquestionscorrect");
				long timeTaken = results.getLong("timetaken");
				if(numQuestionsCorrect > mostQuestionsCorrect || ((numQuestionsCorrect == mostQuestionsCorrect) && timeTaken < bestTimeTaken)){
					String whichQuestionsCorrect = results.getString("whichquestioncorrect");
					info = new ChallengeQuizInfo(timeTaken, numQuestionsCorrect, numQuestions, whichQuestionsCorrect);
				}
			}
			return info;
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return null;
	}

	public void sendChallengeRequest(String challenged, String challenger, int challengeQuizID){
		sendMessage(challenged, challenger, 2, "", challengeQuizID);
	}

	public void sendChallengeResponse(String challenger, String challenged, boolean challengerWon, int challengeID){
		Quiz challengeQuiz = getQuizById(challengeID);
		String messageField = "";
		if(challengerWon)messageField+="true";
		else messageField+="false";
		messageField+=Question.DELIMITER+challengeQuiz.getName();
		sendMessage(challenged,challenger, 3,messageField, challengeID);
	}

	//name, category, description, totalQuestions, multiple, immediate, practice, random
	public int addQuizToDatabase (String name, int category, String description, String instructions, String creator, boolean random,
			boolean onlyOnePage, boolean immediateCorrection){
		try{
			PreparedStatement query = 
				db_con.prepareStatement("INSERT INTO quizzes (name, description, instructions, creator, randomquestion, onlyOnePage, immediateCorrection, category) VALUES (?,?,?,?,?,?,?,?)");
			query.setString(1,name);
			query.setString(2,description);
			query.setString(3,instructions);
			query.setString(4,creator);
			query.setBoolean(5,random);
			query.setBoolean(6,onlyOnePage);
			query.setBoolean(7,immediateCorrection);
			query.setInt(8,category);
			query.execute();

			PreparedStatement getID = 
				db_con.prepareStatement("SELECT id from quizzes order by id desc limit 1");
			ResultSet rs = getID.executeQuery();
			if(rs.next())
				return (int) rs.getInt("id");
			else
				return -1;
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return -1;
	}

	public void insertQuestionIntoDatabase (int quizId, String prompt, int type, String choices, int correctChoice, String answers){
		try{
			PreparedStatement query = 
				db_con.prepareStatement("INSERT INTO questions (quiz, prompt, type, choices, correctchoice, answer) VALUES (?,?,?,?,?,?)");
			query.setInt(1,quizId);
			query.setString(2,prompt);
			query.setInt(3,type);
			query.setString(4,choices);
			query.setInt(5,correctChoice);
			query.setString(6,answers);
			query.execute();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}

	public int numUnreadMessages(String toUsername, String fromUsername){
		try{
			String queryString = "SELECT COUNT(*) As count FROM messages WHERE to_username = ? AND message_seen = ? And from_username = ?";
			queryString += " AND (message_type = 1 OR message_type = 2 OR message_type =3)";
			PreparedStatement query = db_con.prepareStatement(queryString);
			query.setString(1,toUsername);
			query.setBoolean(2,false);
			query.setString(3,fromUsername);
			ResultSet resultSet = query.executeQuery();
			resultSet.next();
			int numUnreadMessages = resultSet.getInt("count");
			return numUnreadMessages;
		}catch(SQLException e){
			e.printStackTrace();
		}
		return -1;
	}
	
	public ArrayList<String> getFriendsRecentHistory(HashSet<String> friends, int limit) {
		ArrayList<String> eventStrings = new ArrayList<String>();

		TreeMap<String, String> eventStringTimes = new TreeMap<String, String>(Collections.reverseOrder());
		for (String friendName: friends) {
			try {
				PreparedStatement query = db_con.prepareStatement("SELECT * FROM quizlog WHERE username = ? ORDER BY timefinished DESC LIMIT 10");
				query.setString(1, friendName);
				ResultSet results = query.executeQuery();
				while(results.next()) {
					int numQuestions = results.getInt("numquestions");
					int numQuestionsCorrect = results.getInt("numquestionscorrect");
					String timeFinished = results.getString("timefinished");
					Quiz q = getQuizById(results.getInt("quiz"));
					String eventString = "";
					eventString += ("<a href = '/cs108/user/account.jsp?username=" + friendName + "'>" + friendName + "</a>");
					eventString += (" finished <a href = '/cs108/QuizServlet?id=" + q.getId() + "'>" + q.getName() + "</a>");
					eventString += (" and got " + numQuestionsCorrect + " out of " + numQuestions + " questions correct.");
					eventStringTimes.put(timeFinished, eventString);

				}
				
				query = db_con.prepareStatement("SELECT * FROM achievements WHERE username = ? ORDER BY timereceived DESC LIMIT 10 ");
				query.setString(1, friendName);
				results = query.executeQuery();
				while(results.next()) {
					Achievement a = Achievement.achievements[results.getInt("achievement")];
					String timeEarned = results.getString("timereceived");
					String eventString = "";
					eventString += ("<a href = '/cs108/user/account.jsp?username=" + friendName + "'>" + friendName + "</a>");
					eventString += " has earned the achievement \"" + a.getAchievementName() + "\"";

					eventStringTimes.put(timeEarned, eventString);

				}
				
				query = db_con.prepareStatement("SELECT * FROM quizzes WHERE creator = ? ORDER BY creationtime DESC LIMIT 10");
				query.setString(1, friendName);
				results = query.executeQuery();
				while(results.next()) {
					int quizId = results.getInt("id");
					String quizName = results.getString("name");
					String creationTime = results.getString("creationtime");
					String eventString = "";
					eventString += ("<a href = '/cs108/user/account.jsp?username=" + friendName + "'>" + friendName + "</a>");
					eventString += (" has created quiz: <a href = '/cs108/QuizServlet?id=" + quizId + "'> " + quizName + "</a>.");

					eventStringTimes.put(creationTime, eventString);
				}
				
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		for (String time : eventStringTimes.keySet()){
			eventStrings.add(eventStringTimes.get(time));
		}
		return eventStrings;
	}
	
	public int getRandomQuizId (){
		try {
			PreparedStatement query = db_con.prepareStatement("SELECT id FROM quizzes ORDER BY rand() limit 1");
			ResultSet results = query.executeQuery();
			if (results.next()) {
				return results.getInt("id");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}
}
