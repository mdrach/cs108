package shared;

import java.util.ArrayList;

import database.DBConnection;

import users.*;
import quiz.*;

public class TableHelper {
	public static String generateQuizTableHTML(ArrayList<QuizInfo> info, DBConnection db_con, boolean categoryIncluded) {
		String html = "";
		html += "<table>";
		html += "<tr>";
		html += "<th> Name </th>";
		html += "<th> Description </th>";
		if (categoryIncluded) {
			html += "<th> Category </th>";
		}
		html += "<th> Creator </th>";
		html += "<th> TimesPlayed </th>";
		html += "</tr>";
		
		for (QuizInfo quiz: info) {
			// Quiz name (links to quiz page)
			html += "<tr>"; 
			html += "<th>"; 
			html += "<a href= '/cs108/QuizServlet?id=" + quiz.id + "'>"; 
			html += quiz.name; 
			html += "</a></th>"; 
			
			
			// Quiz description 
			html += "<th>";
			html += quiz.description;
			html += "</th>";	
			
			// Quiz category (if not on category page, links to category page)
			if (categoryIncluded) {
				html += "<th>";
				html += "<a href = '/cs108/homepage.jsp?category=" + quiz.categoryId + "'/>";
				html += db_con.getCategoryFromId(quiz.categoryId);
				html += "</a></th>";
			}
			
			// Quiz creator (links to creator's account page)
			html += "<th>";
			
			html += "<a href = '/cs108/user/account.jsp?username=" + quiz.creator + "'>"; 
			html += quiz.creator;
			html += "</a></th>"; 
			
			// Number of times the quiz has been taken
			html += "<th>"; 
			html += quiz.timesPlayed;
			html += "</th>"; 
		}
		html += "</table>";
		return html;
	}
	
	public static String generateUserTableHTML(ArrayList<String> usernames) {
		String html = "";
		html += "<table>";
		html += "<tr>";
		html += "<th> Username </th>";
		html += "</tr>";
		for (String username: usernames) {
			html += "<tr>";
			html += "<th>";
			html += "<a href = '/cs108/user/account.jsp?username=" + username + "'>";
			html += username;
			html += "</a></th>";
		}
		html += "</table>";
		return html;
	}
	
	 /*
	<h2>Delete User</h2>
	<form action="/cs108/AdministrationServlet" method="POST">
		Username: <input type=text list='users' name="DeleteUser" required> <br>
	<input type=submit value="Delete User">
	</form>

	<h2>Promote User</h2>
	<form action="/cs108/AdministrationServlet" method="POST">
		Username: <input type=text list='users' name="PromoteUser" required> <br>
	<input type=submit value="Promote User">
	</form>

	<h2>Demote User</h2>
	<form action="/cs108/AdministrationServlet" method="POST">
		Search for user: <input type=text list='users' name="DemoteUser" required> <br>
	<input type=submit value="Demote User">
	</form>
	*/
	
	public static String generateUserAdministrationHTML(DBConnection con, ArrayList<String> usernames) {
		String html = "";
		html += "<table>";
		html += "<tr>";
		html += "<th> Username </th>";
		html += "<th> Delete </th>";
		html += "<th> Promote/Demote </th>";
		html += "</tr>";
		for (String username: usernames) {
			html += "<tr>";
			html += "<th>";
			html += "<a href = '/cs108/user/account.jsp?username=" + username + "'>";
			html += username;
			html += "</a></th>";
			
			html += "<th>";
			html += "<form action='/cs108/AdministrationServlet' method='POST'>";
			html += "<input type=hidden list='users' name='DeleteUser' value='" + username + "'>";
			html += "<input type=submit value='Delete User'>";
			html += "</form>";
			html += "</th>";
			if (!con.isAdmin(username)) {
				html += "<th>";
				html += "<form action='/cs108/AdministrationServlet' method='POST'>";
				html += "<input type=hidden list='users' name='PromoteUser' value='" + username + "'>";
				html += "<input type=submit value='Promote User'>";
				html += "</form>";
				html += "</th>";
			} else {
				html += "<th>";
				html += "<form action='/cs108/AdministrationServlet' method='POST'>";
				html += "<input type=hidden list='users' name='DemoteUser' value='" + username + "'>";
				html += "<input type=submit value='Demote User'>";
				html += "</form>";
				html += "</th>";
			}


			html += "</tr>";

		}
		html += "</table>";
		return html;
	}
	
	public static String generateTopScorersTableHTML(ArrayList<QuizLog> topPerformances) {
		String html = "";
		html += "<table>";
		html += "<tr>";
		html += "<th> User </th>";
		html += "<th> Number of Questions Correct </th>";
		html += "<th> Time Completed </th>";
		html += "</tr>";
		for (QuizLog log: topPerformances) {
			html += "<tr>";
			html += "<th>";
			html += "<a href = '/cs108/user/account.jsp?username=" + log.getUsername() + "'>";
			html += log.getUsername();
			html += "</a></th>";
			html += ("<th>" + log.getNumQuestionsCorrect() + "</th>");
			html += ("<th>" + ((double)log.getTimeTakenToComplete())/1000 + " seconds</th>");
		}
		html += "</table>";
		return html; 
	}
	
	public static String generateQuizHistoryTableHTML(DBConnection db_con, int limit, ArrayList<QuizLog> quizHistory) {
		String html = "";
		html += "<table>";
		html += "<tr>";
		html += "<th> Quiz Name </th>";
		html += "<th> Number of Questions Correct </th>";
		html += "<th> Time Completed </th>";
		html += "</tr>";
		int numLogs = 0;
		for (int i = quizHistory.size() - 1; i >= 0 && numLogs <= limit; i--){
			QuizLog log = quizHistory.get(i);
			html += "<tr>";
			html += "<th>";
			html += "<a href = '/cs108/QuizServlet?id=" + log.getQuizId() + "'>";
			html += db_con.getQuizById(log.getQuizId()).getName();
			html += "</a></th>";
			html += ("<th>" + log.getNumQuestionsCorrect() + "</th>");
			html += ("<th>" + ((double)log.getTimeTakenToComplete())/1000 + " seconds</th>");
			numLogs++;
		}
		html += "</table>";
		return html; 
	}
	
}
