package users;

import quiz.Question;

public class Message implements Comparable<Message> {
	private int messageID;
	private String toUsername;
	private String fromUsername;
	private int messageType;
	private String messageField;
	private boolean message_Seen;
	private int challengeQuizID;
	private String timeSent;
	public static final int messageWidth = 75;
	public static final int FriendRequest = 0;
	public static final int Message = 1;
	public static final int Challenge = 2;
	private static String messageTypes[] = {"Friend Request", "Message", "Challenge", "ChallengeResponse"};
	
	public Message(int messageID, String toUsername, String fromUsername, int messageType, String messageField, 
			boolean message_Seen, int challengeQuizID, String timeSent){
		this.messageID = messageID;
		this.toUsername = toUsername;
		this.fromUsername = fromUsername;
		this.messageType = messageType;
		this.messageField = messageField;
		this.message_Seen = message_Seen;
		this.challengeQuizID = challengeQuizID;
		this.timeSent = timeSent;
	}
	
	public String getToUsername(){
		return toUsername;
	}
	
	public String getFromUsername(){
		return fromUsername;
	}
	
	public String getMessageType(){
		return messageTypes[messageType];
	}

	public String getMessageField(){
		return messageField;
	}
	
	public boolean messageSeen(){
		return message_Seen;
	}

	public int getChallengeQuizID(){
		return challengeQuizID;
	}

	public String getTimeSent(){
		return timeSent;
	}
	
	public int getMessageID(){
		return messageID;
	}

	@Override
	public int compareTo(Message m2) {
		if(this.getTimeSent().compareTo(m2.getTimeSent()) > 0)return 1;
		else if(this.getTimeSent().compareTo(m2.getTimeSent()) < 0)return -1;
		return 0;
	}
	
	public String getHTML(boolean userChanged, String userReading){
		String HTML = "<pre>";
		if(userChanged){
			HTML+= "<hr>";
			HTML+= "<b><font color = cornflowerblue>" + fromUsername + "</font></b>";
		}
		int numPaddingSpaces = messageWidth - timeSent.length();
		if(userChanged)numPaddingSpaces -= fromUsername.length();
		for(int i=0; i<numPaddingSpaces; i++)HTML += " ";
		HTML+=timeSent;
		HTML+="<br>";
		if(messageType == 0){ //Friend Request
			if(userReading.equals(toUsername)){
				HTML+="<br>"+ "<b>" + fromUsername + " sent you a Friend Request!" + "</b>";
				
				String acceptRequestLink = "/cs108/AcceptFriendRequestServlet?requester=" + fromUsername;
				HTML+= "<form action= " + acceptRequestLink +" method='post'>";
				HTML+= "<input type = 'submit' class = 'button' value = 'Accept Friend Request' />";
				HTML+= "</form>";
				
				String rejectRequestLink = "/cs108/DeleteFriendRequestServlet?requester=" + fromUsername + "&redirect=list";
				HTML+= "<form action= " + rejectRequestLink +" method='post'>";
				HTML+= "<input type = 'submit' class = 'button' value = 'Reject Friend Request' />";
				HTML+= "</form>";
			}
			else{
				HTML+="<br>" +"<b>" + "You sent a Friend request to " +toUsername + "</b>!";
				String withdrawRequestLink = "/cs108/DeleteFriendRequestServlet?requester=" + 
				userReading + "&requested="+ toUsername + "&redirect=list";
				HTML+= "<form action= " + withdrawRequestLink +" method='post'>";
				HTML+= "<input type = 'submit' class = 'button' value = 'Withdraw Friend Request' />";
				HTML+= "</form>";
			}
			if(messageField!="")HTML+="<br>" + getMessageFieldHTML();
		}
		else if(messageType == 1){ //Normal Message
			HTML+=getMessageFieldHTML();
		}
		else if(messageType == 2){ //Challenge Request
			if(userReading.equals(toUsername)){
				HTML+="<br>"+ "<b>" + fromUsername + " sent you a Quiz Challenge!" + "</b>";
				String challengeQuizLink = "/cs108/quiz/challengeQuizResults.jsp?id=" + challengeQuizID
					+ "&challenger="+fromUsername;
				HTML+= "<br><a href="+challengeQuizLink+">Click here to try To beat them! </a>";
			}
			else{
				HTML+="<br>" +"<b>" + "You sent a Quiz Challenge to " +toUsername + "!</b>";
			}
			if(messageField!="")HTML+="<br>"+getMessageFieldHTML();
		}
		else if(messageType == 3){
			String []challengeQuizInfo = messageField.split(Question.DELIMITER);
			boolean challengedWon = Boolean.parseBoolean(challengeQuizInfo[0]);
			String quizName = challengeQuizInfo[1];
			if(userReading.equals(toUsername)){ //Challenge was sent to Viewer
				if(!challengedWon){
					HTML+="<br>" +"<b>" + "You beat " + fromUsername + " on the quiz entitled: " + quizName + "!</b>";
				}
				else{
					HTML+="<br>" +"<b>" + "You lost to " + fromUsername + " on the quiz entitled: " + quizName + "!</b>";
				}
			}
			else{
				if(!challengedWon){
					HTML+="<br>" +"<b>" + toUsername + " beat you on the quiz entitled: " + quizName + "!</b>";
				}
				else{
					HTML+="<br>" +"<b>" + toUsername + " lost to you on the quiz entitled: " + quizName + "!</b>";
				}
			}
		}
		HTML+="</pre>";
		return HTML;
	}
	
	private String getMessageFieldHTML(){
		int length = messageField.length();
		String messageFieldHTML ="";
		int indexOfWS = 0;
		for(int i=messageWidth; i<length; i+=messageWidth){
			indexOfWS = i;
			while(indexOfWS > i - messageWidth && messageFieldHTML.charAt(indexOfWS)!=' ' 
			&& messageFieldHTML.charAt(indexOfWS)!='\n' && messageFieldHTML.charAt(indexOfWS)!='\t' ) {
				indexOfWS--;
			}
			if(indexOfWS == i-messageWidth)messageFieldHTML += messageField.substring(i-messageWidth, i);
			else{
				messageFieldHTML += messageField.substring(i-messageWidth, indexOfWS-1) + "<br>";
				i = indexOfWS;
			}
		}
		messageFieldHTML += messageField.substring(indexOfWS, length);
		messageFieldHTML += "<br>";
		return messageFieldHTML;
	}
}

