package users;

import java.util.Collections;
import java.util.List;

public class Conversation {
	private List<Message> messages;
	private String fromUsername;
	private String toUsername;
	
	public Conversation(List<Message> messages, String toUsername, String fromUsername){
		this.messages=messages;
		this.fromUsername=fromUsername;
		this.toUsername=toUsername;
	}
	
	public List<Message> getMessages(){
		return messages;
	}
	
	public String getFromUsername(){
		return fromUsername;
	}
	
	public String getToUsername(){
		return toUsername;
	}
	
	public String getHTML(){
		String HTML = "";
		Collections.sort(messages);
		String currentUser = "";
		boolean userChanged;
		for(Message m : messages){
			userChanged=false;
			if(!currentUser.equals(m.getFromUsername())&&m.getMessageType()=="Message"){
				userChanged = true;
				currentUser = m.getFromUsername();
			}
			HTML += m.getHTML(userChanged, toUsername);
		}
		return HTML;
	}
}
