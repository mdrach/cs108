package users;

import java.util.Collections;
import java.util.List;

public class FriendRequests {
	private List<Message> messages;
	private String toUsername;
	
	public FriendRequests(List<Message> messages, String toUsername){
		this.messages=messages;
		this.toUsername=toUsername;
	}
	
	public List<Message> getMessages(){
		return messages;
	}
	
	public String getToUsername(){
		return toUsername;
	}
	
	public String getHTML(){
		String HTML = "";
		Collections.sort(messages);
		String currentUser = "";
		for(Message m : messages){
			if(!currentUser.equals(m.getFromUsername())&&m.getMessageType()=="Message"){
				currentUser = m.getFromUsername();
			}
			HTML += m.getHTML(false, toUsername);
		}
		return HTML;
	}
}
