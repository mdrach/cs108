package users;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import database.DBConnection;

import quiz.Question;
import quiz.Quiz;
import quiz.QuizInfo;
import quiz.QuizLog;
import achievement.Achievement;

public class User {
	private String username;
	private String encodedPassword;
	private String biography;
	private String profilePicture;
	public HashSet<Integer> achievementIds;
	private ArrayList<Achievement> achievementsEarned;
	// NOTE: This is meant to be a map from a quiz id to the user's best performance on that quiz. If can also
	// be used as a check to see what quizzes the user has taken.
	private HashMap<Integer, QuizLog> bestQuizzes;
	private HashSet<String> friends;
	private ArrayList<QuizInfo> quizzesCreated;
	private ArrayList<QuizLog> quizLogs;
	private int achievementPoints;
	private int privacySetting;
	private String creationTime;
	private boolean admin;
	
	public User(String username, String encodedPassword, String biography, String profilePicture, 
			int privacySetting, String creationTime, boolean admin){
		this.username = username;
		this.encodedPassword = encodedPassword;
		this.biography = biography;
		this.profilePicture = profilePicture;
		this.privacySetting = privacySetting;
		this.creationTime = creationTime;
		this.admin = admin;
		achievementsEarned = new ArrayList<Achievement>();
		bestQuizzes = new HashMap<Integer, QuizLog>();
		friends = new HashSet<String>();
		quizzesCreated = new ArrayList<QuizInfo>();
		quizLogs = new ArrayList<QuizLog>();
		achievementPoints = 0;
	}
	
	
	public void addAchievement(Achievement a) {
		achievementsEarned.add(a);
		achievementPoints += a.getPointValue();
	}
	
	public boolean hasEarnedAchievement(int achievementId) {
		for (int i = 0; i < achievementsEarned.size(); i++) {
			if (achievementsEarned.get(i).getId() == achievementId) return true;
		}
		return false;
	}
	
	public void addQuizLog(QuizLog log) {
		quizLogs.add(log);
		int quizId = log.getQuizId();
		if(bestQuizzes.containsKey(quizId) && bestQuizzes.get(quizId).getNumQuestionsCorrect() < log.getNumQuestionsCorrect()){
			if (bestQuizzes.get(quizId).getNumQuestionsCorrect() < log.getNumQuestionsCorrect()) {
				bestQuizzes.put(quizId, log);
			}
		} else {
			bestQuizzes.put(quizId, log);
		}
	}
	
	public ArrayList<QuizInfo> getQuizzesCreated() {
		return quizzesCreated;
	}
	
	public void addQuizCreated(QuizInfo info) {
		quizzesCreated.add(info);
	}
	
	public void addFriend(String friend) {
		friends.add(friend);
	}
	
	public String getUsername(){
		return this.username;
	}
	
	public String getEncodedPassword(){
		return this.encodedPassword;
	}
	
	public String getBiography() {
		return biography;
	}
	
	public String getProfilePictureURL() {
		return profilePicture;
	}
	
	public HashMap<Integer, QuizLog> getBestQuizzes() {
		return bestQuizzes;
	}
	
	public boolean hasPlayedQuiz(int quizId) {
		boolean result = bestQuizzes.containsKey(quizId);
		return result;
	}
	
	public int getBestScore(int quizId) {
		return bestQuizzes.containsKey(quizId) ? bestQuizzes.get(quizId).getNumQuestionsCorrect() : 0;
	}
	
	public ArrayList<QuizLog> getQuizLogs(){
		return this.quizLogs;
	}
	
	public ArrayList<Achievement> getAchievementsEarned(){
		return achievementsEarned;
	}
	
	// Given a quiz log that is about to be submitted, checks if the user has earned any achievements
	public int checkForAndAddAchievements(DBConnection db_con, QuizLog quizLog) {
		int numAchievements = 0;
		if (bestQuizzes.size() >= 4 && !hasEarnedAchievement(Achievement.QUIZ_HOPPER)) {
			db_con.insertAchievement(Achievement.QUIZ_HOPPER, username);
			numAchievements++;
		}
		HashSet<Integer> categoriesTaken = new HashSet<Integer>();
		for (int quizId: bestQuizzes.keySet()) {
			Quiz q = db_con.getQuizById(quizId);
			categoriesTaken.add(q.getCategoryId());
		}
		if (!hasEarnedAchievement(Achievement.JACK_OF_TRADES) && categoriesTaken.size() >= 4 && !categoriesTaken.contains(db_con.getQuizById(quizLog.getQuizId()).getCategoryId())){
			db_con.insertAchievement(Achievement.JACK_OF_TRADES, username);
			numAchievements++;
		}
		
		if (!hasEarnedAchievement(Achievement.QUIZ_MACHINE) && quizLogs.size() >= 9) {
			db_con.insertAchievement(Achievement.QUIZ_MACHINE, username);
			numAchievements++;
		} else if (!hasEarnedAchievement(Achievement.QUIZ_MANIAC) && quizLogs.size() >= 24) {
			db_con.insertAchievement(Achievement.QUIZ_MANIAC, username);
			numAchievements++;
		}
		if (quizLog.getNumQuestions() == quizLog.getNumQuestionsCorrect()){
			if(!hasEarnedAchievement(Achievement.PERFECTION)) {
				db_con.insertAchievement(Achievement.PERFECTION, username);
				numAchievements++;
			}
			if (!hasEarnedAchievement(Achievement.SPEED_DEMON)) {
				db_con.insertAchievement(Achievement.SPEED_DEMON, username);
				numAchievements++;
			}
		}
		
		
		if (!hasEarnedAchievement(Achievement.THE_GREATEST)) {
			ArrayList<QuizLog> topScorers = db_con.getTopScorers(1, quizLog.getQuizId());
			boolean greatestAchievementEarned = false;
			if (topScorers.size() == 0) {
				greatestAchievementEarned = true;
			} else {
				QuizLog topScore = topScorers.get(0);
				if (quizLog.getNumQuestionsCorrect() > topScore.getNumQuestionsCorrect()) {
					greatestAchievementEarned = true;
				} else if (quizLog.getNumQuestionsCorrect() == topScore.getNumQuestionsCorrect() 
					&& quizLog.getTimeTakenToComplete() < topScore.getNumQuestionsCorrect()) {
					greatestAchievementEarned = true;
				}
			}
			if (greatestAchievementEarned) {
				db_con.insertAchievement(Achievement.THE_GREATEST, username);
				numAchievements++;
			}
		}	
		return numAchievements;
	}
	
	public HashSet<String> getFriends() {
		return friends;
	}

	public boolean isFriends(String username){
		return friends.contains(username);
	}
	
	public void removeFriend(String username){
		friends.remove(username);
	}

	
	public String getAchievementsHTML () {
		HashSet<Integer> achievementIdsEarned = new HashSet<Integer>();
		for (int i = 0; i < achievementsEarned.size(); i++) {
			achievementIdsEarned.add(achievementsEarned.get(i).getId());
		}
		String HTML = "";
		HTML += "<h2> Achievements</h2>";
		HTML += "Total Achievement Points Earned: <b>" + achievementPoints + "</b>";
		HTML += "<div class='content'>";
		HTML += "<table>";
		HTML += "<tr>";
		HTML += "<th> Name </th>";
		HTML += "<th> Description </th>";
		HTML += "<th> Time Earned </th>";
		HTML += "<th> Achievement Points </th>";
		HTML += "</tr>";
		for(Achievement a : Achievement.achievements){
			HTML += "<tr>";
			HTML += "<td>";
			HTML += a.getAchievementName();
			HTML += "</td>";
			HTML += "<td>";
			HTML += a.getDescription();
			HTML += "</td>";
			HTML += "<td>";
			if (achievementIdsEarned.contains(a.getId())) {
				HTML += a.getTimeReceived();
			} else {
				HTML += "Not yet earned!";
			}
			HTML += "</td>";
			HTML += "<td>";
			HTML += a.getPointValue();
			HTML += "</td>";
			HTML += "</tr>";
		}
		HTML+= "</table>";
		HTML += "</div>";
		return HTML;
	}
	
	public int getAchievementPoints(){
		return achievementPoints;
	}

	public int getPrivacySetting(){
		return privacySetting;
	}
	
	public boolean isAdmin(){
		return this.admin;
	}
}
