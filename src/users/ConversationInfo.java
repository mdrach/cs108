package users;

public class ConversationInfo{
	private String timeLastMessageSent;
	private String lastMessageFieldStub;
	private String fromUsername; // TODO: WORK ON THIS CODE. THIS DOESN'T MAKE SENSE
	private String toUsername;
	public static final int stubLength = 50;
	
	public ConversationInfo(String timeLastMessageSent, String lastMessageFieldStub, String toUsername, String fromUsername){
		this.timeLastMessageSent = timeLastMessageSent;
		this.lastMessageFieldStub = lastMessageFieldStub;
		this.fromUsername = fromUsername;
		this.toUsername = toUsername;
	}
	
	public String getTimeLastMessageSent(){
		return this.timeLastMessageSent;
	}
	
	public String getLastMessageFieldStub(){
		return lastMessageFieldStub;
	}
	
	public String getFromUsername(){
		return fromUsername;
	}
	
	public String getToUsername(){
		return toUsername;
	}

	public void setTimeLastMessageSent(String timeLastMessageSent) {
		this.timeLastMessageSent = timeLastMessageSent;
	}

	public void setLastMessageFieldStub(String messageFieldStub) {
		this.lastMessageFieldStub = messageFieldStub;
	}
	
}
