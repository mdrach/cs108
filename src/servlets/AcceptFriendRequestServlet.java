package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DBConnection;
import users.User;

/**
 * Servlet implementation class AcceptFriendRequestServlet
 */
@WebServlet("/AcceptFriendRequestServlet")
public class AcceptFriendRequestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AcceptFriendRequestServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("homepage.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DBConnection db_con = (DBConnection) getServletContext().getAttribute("con"); 
		String currentUser = (String)request.getSession().getAttribute("user");
		String friendRequester = request.getParameter("requester");
		if(!db_con.friends(currentUser, friendRequester)){
			db_con.makeFriends(currentUser, friendRequester);
			db_con.deleteFriendRequest(currentUser, friendRequester);
		}
		String accountPageRedirectURL = "user/account.jsp?username="+friendRequester;
		RequestDispatcher dispatch = request.getRequestDispatcher(accountPageRedirectURL);
		dispatch.forward(request,response);
	}

}
