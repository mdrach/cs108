package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.*;

import achievement.Achievement;

import database.DBConnection;
import quiz.*;

import java.util.ArrayList;

/**
 * Servlet implementation class QuizCreateServlet
 */
@WebServlet("/QuizCreateServlet")
public class QuizCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public QuizCreateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("homepage.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String currentUser = (String)request.getSession().getAttribute("user");

		JSONObject data = new JSONObject(request.getParameter("data"));
		String name = data.getString("name");
		int category = data.getInt("category");
		String instructions = data.getString("instructions");
		String description = data.getString("description");
		int totalQuestions = data.getInt("totalQuestions");
		boolean multiple = data.getString("multi").equals("false");
		boolean immediate = data.getString("immediate").equals("true");
		boolean random = data.getBoolean("random");
		JSONArray questions = data.getJSONArray("questions");
		DBConnection con = (DBConnection)request.getServletContext().getAttribute("con");
		int quizID = con.addQuizToDatabase(name, category, description, instructions, currentUser, random, multiple, immediate);
		sendQuestionsToDatabase(con, quizID, questions);
		
		int numQuizzesCreated = con.getUserByUsername(currentUser).getQuizzesCreated().size();
		if (numQuizzesCreated == 1) {
			con.insertAchievement(Achievement.AMATEUR_AUTHOR, currentUser);
			request.getSession().setAttribute("achievement", 1);
		} else if (numQuizzesCreated == 5) {
			con.insertAchievement(Achievement.PROLIFIC_AUTHOR, currentUser);
			request.getSession().setAttribute("achievement", 1);
		} else if (numQuizzesCreated == 10) {
			con.insertAchievement(Achievement.PRODIGIOUS_AUTHOR, currentUser);
			request.getSession().setAttribute("achievement", 1);
		}

		request.getSession().setAttribute("quizCreate", "Quiz entitled: " + name + " sucessfuly created");
		

	}
	

	private void sendQuestionsToDatabase(DBConnection db_con, int quizId, JSONArray questions) {
		for (int i = 0; i < questions.length(); i++) {
			JSONObject question = questions.getJSONObject(i);
			String typeString = question.getString("type");
			int type;
			String prompt = "";
			if (!typeString.equals("picture-response")) {
				prompt = question.getString("text");
			}
			ArrayList<String> choices = null;
			ArrayList<String> answers = new ArrayList<String>();
			int correctChoice = -1;
			// change question to have default null for choices and correctchoice
			if(typeString.equals("multiple-choice")){
				type = 3;
				JSONArray choicesArr = question.getJSONArray("answerTextArr");
				choices = new ArrayList<String>();
				for (int j = 0; j < choicesArr.length(); j++) {
					choices.add(choicesArr.getString(j));
				}
				correctChoice = question.getInt("answer");
				answers.add(choices.get(correctChoice));
			} else if (typeString.equals("question-response")) {
				type = 1;
				JSONArray answersArr = question.getJSONArray("answertext");
				for (int j = 0; j < answersArr.length(); j++) {
					answers.add(answersArr.getString(j).toLowerCase());
				}
			} else if (typeString.equals("fill-in-the-blank")) {
				type = 2;
				JSONArray answersArr = question.getJSONArray("answer");
				for (int j = 0; j < answersArr.length(); j++) {
					answers.add(answersArr.getString(j).toLowerCase());
				}
				
			} else if (typeString.equals("picture-response")) {
				type = 4;
				prompt = question.getString("url");
				JSONArray answersArr = question.getJSONArray("answertext");
				for (int j = 0; j < answersArr.length(); j++) {
					answers.add(answersArr.getString(j).toLowerCase());
				}
			} else {
				continue;
			}
			
			String choicesString = "";
			if (choices != null) {  
				for (String choice : choices){
					choicesString += choice + Question.DELIMITER;
				}
			}
				
			choicesString = choicesString.replaceAll(Question.DELIMITER + "$", "");

			
			String answersString = "";
			for (String answer : answers){
				answersString += answer + Question.DELIMITER;
			}
			answersString = answersString.replaceAll(Question.DELIMITER + "$", "");

			db_con.insertQuestionIntoDatabase(quizId, prompt, type, choicesString, correctChoice, answersString);
		}
	}

}
