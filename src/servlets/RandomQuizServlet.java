package servlets;

import java.io.IOException;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DBConnection;

/**
 * Servlet implementation class RandomQuiz
 */
@WebServlet("/RandomQuizServlet")
public class RandomQuizServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RandomQuizServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DBConnection con = (DBConnection) request.getServletContext().getAttribute("con");
        int numQuizzes = con.getNumQuizzes();
        if (numQuizzes < 1){
        	RequestDispatcher dispatch = request.getRequestDispatcher("homepage.jsp");
        	dispatch.forward(request, response);
        }
        int randomQuiz = con.getRandomQuizId();
    	RequestDispatcher dispatch = request.getRequestDispatcher("QuizServlet?id=" + randomQuiz);
    	dispatch.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
