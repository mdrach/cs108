package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import users.Conversation;
import database.DBConnection;

/**
 * Servlet implementation class ConversationServlet
 */
@WebServlet("/ConversationServlet")
public class ConversationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConversationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter writer = response.getWriter();
		DBConnection con = (DBConnection)request.getServletContext().getAttribute("con");
		String toUsername = (String)request.getSession().getAttribute("user");
		String fromUsername = (String)request.getSession().getAttribute("conversationUser");
		con.readAllMessages(toUsername, fromUsername);
		Conversation conversation = con.getConversation(toUsername ,fromUsername);
		writer.print(conversation.getHTML());
		writer.close();
	}

}
