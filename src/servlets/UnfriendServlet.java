package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DBConnection;

/**
 * Servlet implementation class UnfriendServlet
 */
@WebServlet("/UnfriendServlet")
public class UnfriendServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UnfriendServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("homepage.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DBConnection db_con = (DBConnection) getServletContext().getAttribute("con"); 
		String unfriender = request.getParameter("unfriender");
		String unfriended = request.getParameter("unfriended");
		db_con.deleteFriendShip(unfriender,unfriended);
		String accountPageRedirectURL = "/user/account.jsp?username="+unfriended;
		RequestDispatcher dispatch = request.getRequestDispatcher(accountPageRedirectURL);
		dispatch.forward(request,response);
	}

}
