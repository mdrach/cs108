package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DBConnection;

/**
 * Servlet implementation class numUnreadMessagesServlet
 */
@WebServlet("/numUnreadMessagesServlet")
public class numUnreadMessagesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public numUnreadMessagesServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter writer = response.getWriter();
		DBConnection con = (DBConnection)request.getServletContext().getAttribute("con");
		String currentUser = (String)request.getSession().getAttribute("user");
		int numUnreadMessages = con.numUnreadMessages(currentUser);
		int numUnseenFriendRequests = con.numUnseenFriendRequests(currentUser);
		if(numUnreadMessages > 0)writer.print("<font color = cornflowerblue>");
		writer.print("<a href='/cs108/user/conversations.jsp'>"+numUnreadMessages +" Unread Messages"+"</a>");
		writer.print("  ");
		if(numUnreadMessages > 0 && numUnseenFriendRequests == 0)writer.print("</font>");
		else if(numUnreadMessages == 0 && numUnseenFriendRequests > 0)writer.print("<font color = cornflowerblue>");
		writer.print(" | <a href='/cs108/FriendsRequestFeedServletRedirect'>"+numUnseenFriendRequests +" Unseen Friend Requests"+"</a></li>");
		if(numUnseenFriendRequests > 0)writer.print("</font>");
		writer.close();
	}

}
