package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DBConnection;

/**
 * Servlet implementation class ChangePasswordServlet
 */
@WebServlet("/AccountManagementServlet")
public class AccountManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AccountManagementServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("homepage.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DBConnection con = (DBConnection)request.getServletContext().getAttribute("con");

		// User is trying to change password.
		if(request.getParameter("oldpassword") != null){
			String oldpassword = request.getParameter("oldpassword");
			String newpassword = request.getParameter("newpassword");
			String user = (String)request.getSession().getAttribute("user");
			if (con.validLogin(user, oldpassword)){
				con.changePassword(user, newpassword);
				request.setAttribute("message", "Password update successful.");
			} else {
				request.setAttribute("message", "Password update failed.  Please enter your correct current password.");
			}
		}
		
		else if(request.getParameter("biography") != null){
			String biography = request.getParameter("biography");
			String user = (String)request.getSession().getAttribute("user");
			con.changeBiography(user, biography);
			request.setAttribute("message", "Biography update successful.");
		}
		
		else if(request.getParameter("picture") != null){
			String picture = request.getParameter("picture");
			String user = (String)request.getSession().getAttribute("user");

			con.changePicture(user, picture);
			request.setAttribute("message", "Picture update successful.");
		}
		else if(request.getParameter("messagingprivacy") != null){
			String user = (String)request.getSession().getAttribute("user");
			int messagingPrivacySetting = Integer.parseInt(request.getParameter("messagingprivacy"));
			con.changeMessagingPrivacySetting(user, messagingPrivacySetting);
			request.setAttribute("message", "Messaging Privacy update successful.");
		}
		else if(request.getParameter("friendrequestprivacy") != null){
			String user = (String)request.getSession().getAttribute("user");
			int friendRequestPrivacySetting = Integer.parseInt(request.getParameter("friendrequestprivacy"));
			con.changeFriendRequestPrivacySetting(user, friendRequestPrivacySetting);
			request.setAttribute("message", "Friend Request Privacy update successful.");
		}

		request.getRequestDispatcher("user/manage_account.jsp").forward(request, response);
	}

}
