package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DBConnection;

/**
 * Servlet implementation class ChallengeUserToQuizServlet
 */
@WebServlet("/ChallengeUserToQuizServlet")
public class ChallengeUserToQuizServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChallengeUserToQuizServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("homepage.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DBConnection db_con = (DBConnection) getServletContext().getAttribute("con"); 
		String challenger = (String)request.getSession().getAttribute("user");
		String challenged = request.getParameter("challenged");
		if (db_con.userExists(challenged)){
			int quizID = Integer.parseInt(request.getParameter("id"));
			db_con.sendChallengeRequest(challenged, challenger, quizID);
			String conversationRequestPage = "user/conversation.jsp?fromUser="+challenged;
			response.sendRedirect(conversationRequestPage);
		} else {
			String conversationRequestPage = "user/conversation.jsp";
			request.setAttribute("message", "User " + challenged + " does not exist.  No challenge request sent.");
			response.sendRedirect(conversationRequestPage);
		}
	}

}
