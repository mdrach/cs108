package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import quiz.Quiz;
import quiz.QuizLog;

import database.DBConnection;

/**
 * Servlet implementation class QuizServlet
 */
@WebServlet("/QuizServlet")
public class QuizServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public QuizServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    /* Called when the user first selects a quiz from his homepage, or if he navigates. */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Get the database connection to interact with the database.
		DBConnection con = (DBConnection)request.getServletContext().getAttribute("con");
		
        String quizid = request.getParameter("id");
        
        // Redirect to homepage if no argument
        if (quizid == null) {
    		RequestDispatcher dispatch = request.getRequestDispatcher("homepage.jsp");
    		dispatch.forward(request, response);
        }
        Quiz quiz = con.getQuizById(Integer.parseInt(quizid));
		request.getSession().setAttribute("quiz", quiz);
				
        // Forward request to the quiz quiz.jsp
		RequestDispatcher dispatch = request.getRequestDispatcher("quiz/instructions.jsp");
		dispatch.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	/* Called when the user presses "start quiz" on instructions.jsp. */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Quiz quiz = (Quiz) request.getSession().getAttribute("quiz");		

		/* Create QuizLog to record the results of this quiz. */
		QuizLog quizLog = new QuizLog ((String) request.getSession().getAttribute("user"), 
				quiz.getId(), quiz.getNumQuestions(), System.currentTimeMillis());

		request.getSession().setAttribute("quizLog", quizLog);
		request.getSession().setAttribute("currentQuestion", 1);
		String challengeRequest = request.getParameter("challengeRequest");
		String quizURL;
		if(quiz.isOnlyOnePage())quizURL = "quiz/quiz_one_page.jsp";
		else quizURL = "quiz/quiz_multi_page.jsp";
		if(challengeRequest!=null)quizURL += "?challengeRequest="+challengeRequest;
		RequestDispatcher dispatch = request.getRequestDispatcher(quizURL);
		dispatch.forward(request,response);
	}

}
