package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import users.User;

import database.DBConnection;

/**
 * Servlet implementation class CreateAccountServlet
 */
@WebServlet("/CreateAccountServlet")
public class CreateAccountServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateAccountServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatch = request.getRequestDispatcher("create_account.html");
		dispatch.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DBConnection con = (DBConnection)request.getServletContext().getAttribute("con");
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		
		if (con.userExists(name)) {
			RequestDispatcher dispatch = request.getRequestDispatcher("already_exists.jsp");
			dispatch.forward(request, response);
		} else {


			con.createNewAccount(name, password);
			RequestDispatcher dispatch = request.getRequestDispatcher("homepage.jsp?first=true");
			request.getSession().setAttribute("user", name);
			dispatch.forward(request, response);
		}
	}

}
