package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DBConnection;

import quiz.Question;
import quiz.Quiz;
import quiz.QuizLog;

/**
 * Servlet implementation class MultiPageSubmit
 */
@WebServlet("/MultiPageSubmitServlet")
public class MultiPageSubmitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MultiPageSubmitServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("homepage.jsp").forward(request, response);
	}
	

	private String getQuestionAnswer (Question question, HttpServletRequest request) {
		int id = question.getId();

		//This array should be length 1, containing the value that the user submitted for this question. */
		String[] parameterValues = request.getParameterValues(Integer.toString(id));

		// User did not respond to question
		if (parameterValues == null)
			return "";
		else {
			assert(request.getParameterValues(Integer.toString(id)).length == 1);			
			String userAnswer = parameterValues[0];
			return userAnswer;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	/* Grades the previous question, then forwards the request. */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* Get the QuizLog for storing values. */
		QuizLog quizLog = (QuizLog) request.getSession().getAttribute("quizLog");
		
		/* Get current question number. */
		int questionNum = (Integer) request.getSession().getAttribute("currentQuestion");
		
		/* Get data to store in QuizLog. */
		quizLog.setTimeFinished(System.currentTimeMillis());
		Quiz quiz = (Quiz) request.getSession().getAttribute("quiz");

		Question question = quiz.getQuestion(questionNum - 1);
		String userResponse = getQuestionAnswer (question, request);
		quizLog.addResponse(userResponse);
		if(question.answerCorrect(userResponse))
			quizLog.addCorrect(question.getId());

		if(questionNum >= quiz.getNumQuestions()){
			DBConnection con = (DBConnection)request.getServletContext().getAttribute("con");
			int numAchievementsEarned = con.storeQuizResult(quizLog);
			if(numAchievementsEarned>0)request.getSession().setAttribute("achievement", numAchievementsEarned);
		}
		request.getSession().setAttribute("currentQuestion", ++questionNum);
		
		String dispatchPage = "quiz/quiz_single_page_results.jsp";
		if(request.getParameter("challengeRequest")!=null)dispatchPage+="?challengeRequest="+request.getParameter("challengeRequest");
		RequestDispatcher dispatch = request.getRequestDispatcher(dispatchPage);
		dispatch.forward(request, response);
	}

}
