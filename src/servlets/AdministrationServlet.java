package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DBConnection;

/**
 * Servlet implementation class AdministrationServlet
 */
@WebServlet("/AdministrationServlet")
public class AdministrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdministrationServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("homepage.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DBConnection con = (DBConnection)request.getServletContext().getAttribute("con");
		String currentUser = (String)request.getSession().getAttribute("user");
		// User is trying to change password.
		if(request.getParameter("Announcement") != null){
			String announcement = request.getParameter("Announcement");
			con.setAnnouncement(announcement);
			request.setAttribute("message", "Homepage announcement updated.");
		}

		else if (request.getParameter("SearchUser") != null){
			String userSearch = request.getParameter("SearchUser");
			request.setAttribute("userSearch", userSearch);
		}

		else if(request.getParameter("DeleteUser") != null){

			String user = request.getParameter("DeleteUser");
			if(!con.userExists(user)) {
				request.setAttribute("message", user + " does not exist.  No changes made.");
			}
			else if (currentUser.equals(user)){
				request.setAttribute("message", "You cannot delete your own account.  No changes made.");
			}
			else {
				con.deleteUser(user);
				request.setAttribute("message", "User " + user + " has been deleted.");
			}
		}

		else if(request.getParameter("PromoteUser") != null){

			String user = request.getParameter("PromoteUser");
			if(!con.userExists(user)) {
				request.setAttribute("message", user + " does not exist.  No changes made.");
			}
			else if (con.isAdmin(user)){
				request.setAttribute("message", user + " is already an administrator.  No changes made.");
			}
			else {
				con.promoteUser(user);
				request.setAttribute("message", user + " is now an administrator.");
			}
		}

		else if(request.getParameter("DemoteUser") != null){
			String user = request.getParameter("DemoteUser");
			if(!con.userExists(user)) 
				request.setAttribute("message", user + " does not exist.  No changes made.");
			else if (!con.isAdmin(user))
				request.setAttribute("message", user + " is already not an administrator.   No changes made.");
			else if (currentUser.equals(user)){
				request.setAttribute("message", "You cannot demote your own account.  No changes made.");
			}
			else {
				con.demoteUser(user);
				request.setAttribute("message", user + " is no longer an administrator.");
			}
		}

		else if(request.getParameter("DeleteQuiz") != null){
			int quizId = Integer.parseInt(request.getParameter("DeleteQuiz"));
			con.deleteQuizByID(quizId);
			request.setAttribute("message", "Quiz deleted.");
		}
		
		request.getRequestDispatcher("administration.jsp").forward(request, response);
	}

}
