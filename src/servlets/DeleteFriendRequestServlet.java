package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DBConnection;

/**
 * Servlet implementation class DeleteFriendRequestServlet
 */
@WebServlet("/DeleteFriendRequestServlet")
public class DeleteFriendRequestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteFriendRequestServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("homepage.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DBConnection db_con = (DBConnection) getServletContext().getAttribute("con"); 
		String currentUser = (String)request.getSession().getAttribute("user");
		String requested = request.getParameter("requested");
		if(requested==null)requested= currentUser;
		String friendRequester = request.getParameter("requester");
		db_con.deleteFriendRequest(requested, friendRequester);
		String accountPageRedirectURL; 
		if(request.getParameter("redirect")!=null)accountPageRedirectURL= "user/friend_requests.jsp";
		else accountPageRedirectURL= "user/account.jsp?username="+friendRequester;
		
		response.sendRedirect(accountPageRedirectURL);
	}

}
