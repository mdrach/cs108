package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DBConnection;

import quiz.Question;
import quiz.Quiz;
import quiz.QuizLog;

/**
 * Servlet implementation class SinglePageSubmit
 */
@WebServlet("/SinglePageSubmitServlet")
public class SinglePageSubmitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SinglePageSubmitServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("homepage.jsp").forward(request, response);
	}

	private String getQuestionAnswer (Question question, HttpServletRequest request) {
		int id = question.getId();

		//This array should be length 1, containing the value that the user submitted for this question. */
		String[] parameterValues = request.getParameterValues(Integer.toString(id));

		// User did not respond to question
		if (parameterValues == null)
			return "";
		else {
			assert(request.getParameterValues(Integer.toString(id)).length == 1);			
			String userAnswer = parameterValues[0];
			return userAnswer;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* Get the QuizLog for storing values. */
		QuizLog quizLog = (QuizLog) request.getSession().getAttribute("quizLog");

		/* Get data to store in QuizLog. */
		quizLog.setTimeFinished(System.currentTimeMillis());
		Quiz quiz = (Quiz) request.getSession().getAttribute("quiz");

		/* Extract user quiz answers from request object for grading. */
		for(Question question : quiz.getQuestions()) {
			String userResponse = getQuestionAnswer (question, request);
			quizLog.addResponse(userResponse);
			if(question.answerCorrect(userResponse))
				quizLog.addCorrect(question.getId());
		}

		/* Grade the quiz to fill a QuizLog object. */
		DBConnection con = (DBConnection)request.getServletContext().getAttribute("con");
		int numAchievementsEarned = con.storeQuizResult(quizLog);
		if(numAchievementsEarned>0)request.getSession().setAttribute("achievement", numAchievementsEarned);
		String resultsURL = "quiz/quiz_results.jsp";
		if(request.getParameter("challengeRequest")!=null)resultsURL+="?challengeRequest=true";
		RequestDispatcher dispatch = request.getRequestDispatcher(resultsURL);
		dispatch.forward(request, response);
	}

}
