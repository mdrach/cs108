package quiz;

import java.util.ArrayList;

public class ResponseQuestion extends Question {
	String prompt;
	String[] possibleAnswers;

	public ResponseQuestion(int id, int quizId) {
		super(id, quizId);
		type = QUESTION_TYPE_RESPONSE;
	}

	public ResponseQuestion(int id, int quizId, String prompt, String answerString){
		this(id, quizId);
		this.prompt = prompt;
		possibleAnswers = answerString.split(DELIMITER);
	}

	@Override 
	public String[] getAnswers() {
		return possibleAnswers;
	}

	@Override 
	public String getPrompt() {
		return prompt;
	}

	@Override 
	public boolean answerCorrect(Object answer) {
		// TODO: Should this be lowercase by default? Case insensitivity
		String lAnswer = ((String)answer).toLowerCase().trim();
		for (int i = 0; i < possibleAnswers.length; i++) {
			if (lAnswer.equals(possibleAnswers[i].toLowerCase().trim())) return true;
		}
		return false;
	}

	@Override
	public String getHTML() {
		String HTML = "<div class='response'>";
		HTML+= "<div class='question-text'>";
		HTML+= prompt;
		HTML+= "</div>";
		HTML+= "<br>";
		//HTML+= "<form name='"+getId()+"'>";
		HTML+= "<input type='text' name='"+getId()+"' value=''>";
		//HTML+="<label>Answer: </label><br>";
		//HTML+= "</form>";
		HTML+= "</div>";
		return HTML;
	}

	@Override
	public String getCorrectedHTML(String userAnswer, boolean correct) {
		String HTML = "<div class='response'>";
		HTML+= "<div class='question-text'>";
		HTML+= prompt;
		HTML+= "</div>";
		HTML+= "<br>";
		HTML+= "<input type='text' name='' value='"+userAnswer+"' readonly> (";
		if (correct){				
			HTML+= "<span style='background-color:"+Question.correctAnswerColor+"'>";
			HTML+= "CORRECT! :-D)";
			HTML+="</span>";
		} else {
			int i = 0;
			for(String possibleAnswer : possibleAnswers){
				HTML+= "<span style='background-color:" +Question.correctedAnswerColor+"'>";
				HTML+= possibleAnswer;
				HTML+="</span>";
				if(++i!=possibleAnswers.length)HTML += " or ";
			}
			HTML+=")";
		}
		HTML+= "</div>";
		return HTML;
	}
}
