package quiz;

abstract public class Question {
	public static final int QUESTION_TYPE_UNSPECIFIED = 0;
	public static final int QUESTION_TYPE_RESPONSE = 1;
	public static final int QUESTION_TYPE_BLANK = 2;
	public static final int QUESTION_TYPE_MULTIPLE = 3;
	public static final int QUESTION_TYPE_PICTURE = 4;
	
	// TODO: Change this so that it isn't as much of a hack, or find the best delimiter?
	public static final String DELIMITER = "@&@";
	
	
	private int id;
	private int quizId;
	protected int type;
	public static final String correctedAnswerColor = "#FE2E2E";
	public static final String correctAnswerColor = "#00FF00";


	public Question(int id, int quizId) {
		this.id = id;
		this.quizId = quizId;
		type = QUESTION_TYPE_UNSPECIFIED;
	}
	
	public int getType() {
		return type;
	}
	
	public int getQuizId() {
		return quizId;
	}
	
	
	/**
	 * To be overrided: Returns a boolean as to whether or not the passed in answer is correct.
	 * 
	 * @param answer Note that this is an object- could be any type given that different
	 * subclasses have different answer formats
	 * @return Boolean for whether or not the answer is correct
	 */
	abstract public boolean answerCorrect(Object answer);
	
	/**
	 * To be overrided: Returns a Object which contains all of the possible valid answers for a question
	 * 
	 * @param answer Note that this is an object- could be any type given that different
	 * subclasses have different answer formats
	 * @return Boolean for whether or not the answer is correct
	 */
	abstract public Object getAnswers();
	
	/**
	 * 
	 * @return Returns the question's id as contained in the SQL database
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * To be overrided: Returns the question "prompt" for the question. Note that in 
	 * some cases this may not be well-defined.
	 * 
	 * @return String for the question "prompt" or description
	 */
	abstract public String getPrompt();

	
	
	/**
	 * To be overrided: Returns the HTML to display the question. By default it returns the empty string
	 * 
	 */
	abstract public String getHTML();
	
	/**
	 * To be overrided: Returns the HTML to display the corrected question.
	 * 
	 * @return
	 */
	abstract public String getCorrectedHTML(String userAnswer, boolean correct);
}
