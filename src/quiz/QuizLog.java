package quiz;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class QuizLog {
	private String username;
	private int quizId;
	private long timeFinished;
	private long timeStarted;
	private long timeTakenToComplete;
	private Set<Integer> whichQuestionsCorrect;
	private ArrayList<String> userResponses;
	private int numQuestionsCorrect;
	private int numQuestions;
	
	
	/* Used to create quiz at creation, stores quiz result information in steps. */
	public QuizLog (String username, int quizId, int numQuestions, long timeStarted){
		this(username, quizId, numQuestions, new ArrayList<String>(), new HashSet<Integer>(), timeStarted, -1);
	}
	
	/* Used to create a quizlog from the database, lacks certain information that really shouldn't be necessary */
	public QuizLog(String username, int quizId, int numQuestions, int numQuestionsCorrect, Set<Integer> questionsCorrect, long timetaken, long timefinished) {
		this(username, quizId, numQuestions, new ArrayList<String>(), questionsCorrect, timefinished - timetaken, timefinished);
		timeTakenToComplete = timetaken;
	}
	
	public QuizLog(String username, int quizId, int numQuestions, ArrayList<String> responses, Set<Integer> questionsCorrect, 
			long timeStarted, long timeFinished){
		this.username = username;
		this.quizId = quizId;
		this.timeFinished= timeFinished;
		this.timeStarted = timeStarted;
		this.userResponses = responses;
		this.numQuestionsCorrect = questionsCorrect.size();
		this.numQuestions = numQuestions;
		this.whichQuestionsCorrect = questionsCorrect;
	}
	
	public String getUsername(){
		return this.username;
	}
	
	public int getQuizId(){
		return this.quizId;
	}
	
	public long getTimeFinished(){
		return this.timeFinished;
	}
	
	public void setTimeFinished(long time){
		this.timeFinished = time;
		this.timeTakenToComplete = this.timeFinished - this.timeStarted;
	}
	
	public void addResponse(String response){
		userResponses.add(response);
	}
	
	public long getTimeStarted(){
		return this.timeStarted;
	}
	
	public long getTimeTakenToComplete(){
		return this.timeTakenToComplete;
	}
	
	public Set<Integer> getWhichQuestionsCorrect(){
		return whichQuestionsCorrect;
	}
	
	public int getNumQuestionsCorrect(){
		return numQuestionsCorrect;
	}
   
	public int getNumQuestions(){
		return this.numQuestions;
	}
	
	public ArrayList<String> getUserResponses(){
		return userResponses;
	}
	
	public void addCorrect (int questionID){
		whichQuestionsCorrect.add(questionID);
		numQuestionsCorrect = whichQuestionsCorrect.size();
	}
	
	public String getMostRecentUserResponse (){
		return userResponses.get(userResponses.size() - 1);
	}

}
