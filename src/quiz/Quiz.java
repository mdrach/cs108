package quiz;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* Max - An instance of the quiz object is made whenever the user begins to take a quiz. 
 * In addition to storing quiz information, it stores information about the current user's quiz session --
 * for example, the question number that the user is currently on. */
public class Quiz {
	private int id;
	private String name;
	private String description;
	private String instructions;
	private String creator;
	private ArrayList<Question> questions;
	private int categoryId;
	
	private boolean randomizedOrder;
	private boolean onlyOnePage;
	private boolean immediateCorrection;
	// Max - Stores the current question of the user.  
	// Necessary to keep track of the user's progress in multi-page quizzes.
	private int questionIndex;
	
	public Quiz(int id, String name, String description, String instructions, String creator, 
			boolean randomizedOrder, boolean onePage, boolean immediateCorrection, int category) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.instructions = instructions;
		this.creator = creator;
		this.randomizedOrder = randomizedOrder;
		this.onlyOnePage = onePage;
		this.immediateCorrection = immediateCorrection;
		questionIndex = 0;
		questions = new ArrayList<Question>();
		this.categoryId = category;
	}
	
	public void addQuestion(Question q) {
		questions.add(q);
	}
	
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public String getInstructions() {
		return this.instructions;
	}
	
	public String getCreator() {
		return creator;
	}
	
	public ArrayList<Question> getQuestions() {
		return questions;
	}
	
	public Question getQuestion(int index) {
		return questions.get(index);
	}
	
	public int getCategoryId() {
		return categoryId;
	}
	
	
	public boolean isRandomizedOrder() {
		return randomizedOrder;
	}
	
	public boolean isOnlyOnePage() {
		return onlyOnePage;
	}
	
	public boolean isImmediateCorrection() {
		return immediateCorrection;
	}

	
	public int questionIndex() {
		return questionIndex;
	}
	
	public int getNumQuestions() {
		return questions.size();
	}
	
	public void randomizeQuestions () {
		Collections.shuffle(questions);
	}
	

}
