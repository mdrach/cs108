package quiz;

import java.util.ArrayList;

public class MultipleChoiceQuestion extends Question {
	private String[] choices;
	private String prompt, answer;
	
	public MultipleChoiceQuestion(int id, int quizId) {
		super(id, quizId);
		type = QUESTION_TYPE_MULTIPLE;
	}

	public MultipleChoiceQuestion(int id, int quizId, String prompt, String choicesString, int answerIndex){
		this(id, quizId);
		this.prompt = prompt;
		this.choices = choicesString.split(DELIMITER);
		answer = choices[answerIndex-1].toLowerCase().trim();
	}

	public String getAnswers() {
		return answer;
	}

	public String getPrompt() {
		return prompt;
	}

	public String[] getChoices(){
		return choices;
	}
	
	public boolean answerCorrect(Object userAnswer) {
		if(answer.equals(((String)userAnswer).toLowerCase().trim())) return true;
		return false;
	}

	public String getHTML(){
		String HTML = "<div class='multiple-choice'>";
		HTML+= "<div class='question-text'>";
		HTML+= prompt;
		HTML+= "</div>";
		HTML+= "<br>";
		//HTML+= "<form name='"+getId()+"'>";
		for (String a : choices){
			//TODO: FIX THIS, names are not correct.
			HTML+= "<input type='radio' name='"+getId()+"' value='"+a+"'>";
			HTML+="<label>"+a+"</label><br>";
		}
		//HTML+= "</form>";
		HTML+= "</div>";
		return HTML;
	}
	
	
	@Override
	public String getCorrectedHTML(String userAnswer, boolean correct){
		String HTML = "<div class='multiple-choice'>";
		HTML+= "<div class='question-text'>";
		HTML+= prompt;
		HTML+= "</div>";
		HTML+= "<br>";
		for (String a : choices){
			boolean choiceCorrect = answerCorrect(a);
			if(choiceCorrect) {
				if (!correct)
					HTML+= "<span style='background-color:"+Question.correctedAnswerColor+"'>";
				else
					HTML+= "<span style='background-color:"+Question.correctAnswerColor+"'>";
			}
			HTML+= "<input type='radio' value='"+a;
            if(userAnswer.equals(a)){ 
            	HTML+="' checked='checked'>";
            }
            else HTML+="' disabled>";
			HTML+="<label>"+a+"</label><br>";
			if(choiceCorrect) HTML+="</span>";
		}
		HTML+= "</div>";
		return HTML;
	}
}
