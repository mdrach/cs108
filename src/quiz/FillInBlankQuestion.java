package quiz;

import java.util.ArrayList;
import java.util.Arrays;

//TODO: Figure out how blank questions will be stored in database. Literal blank characters, or what?
public class FillInBlankQuestion extends Question {
//	private static final String regex = "@&@"; 
	String prompt;
	String[] possibleAnswers;
	String beforeBlank;
	String afterBlank;
	
	
	public FillInBlankQuestion(int id, int quizId) {
		super(id, quizId);
		type = QUESTION_TYPE_BLANK;
	}
	
	public FillInBlankQuestion(int id, int quizId, String prompt, String answerString){
		this(id, quizId);
		this.prompt = prompt;
		this.possibleAnswers = answerString.split(DELIMITER);
	}
	
	@Override
	public boolean answerCorrect(Object answer) {
		// TODO: Should this be lowercase by default? Case insensitivity
		String lAnswer = ((String)answer).toLowerCase().trim();
		for (int i = 0; i < possibleAnswers.length; i++) {
			if(lAnswer.equals(possibleAnswers[i].toLowerCase().trim())) return true;
		}
		return false;
	}

	@Override
	public Object getAnswers() {
		return possibleAnswers;
	}

	@Override
	public String getPrompt() {
		return prompt;
	}

	@Override
	public String getHTML() {
		//Get the text of the question
		ArrayList<String> textArray = new ArrayList<String>(Arrays.asList(prompt.split(DELIMITER)));
		
		//HTML for the blank
		String blankHtml ="<input type='text' name='"+getId()+"' value=''>";
		
		//Setup header HTML for question
		String HTML = "<div class='fill-in-the-blank'>";
		HTML+= "<div class='question-text'>";
		
		//iterate over the text of the question, adding it and the blanks to the HTML.
		//don't add blank at the start of the question
		boolean addBlank = false;
		for (String text : textArray){
			if(addBlank){
				HTML+=blankHtml;
			} else {
				addBlank = true;
			}
			HTML+= text;
		}
		HTML+= "</div></div>";
		return HTML;
	}

	@Override
	public String getCorrectedHTML(String userAnswer, boolean correct){
		String beforeBlank = prompt.substring(0, prompt.indexOf(DELIMITER) - 1);
		String afterBlank = prompt.substring(prompt.indexOf(DELIMITER) + DELIMITER.length(), prompt.length() - 1);
		String HTML = "<div class='fill-in-blank'>";
		HTML+= "<div class='question-text'>";
		HTML+= "</div>";
		HTML+= "<br>";
		HTML+= beforeBlank + " <input type='text' name='' value='"+userAnswer+"' readonly> (";
		if (correct){				
			HTML+= "<span style='background-color:"+Question.correctAnswerColor+"'>";
			HTML+= "CORRECT! :-D)";
			HTML+="</span>";
		} else {  //print corrections
			int i = 0;
			for(String possibleAnswer : possibleAnswers){
				HTML+= "<span style='background-color:"+Question.correctedAnswerColor+"'>";
				HTML+= possibleAnswer;
				HTML+="</span>";
				if(++i!=possibleAnswers.length) HTML += " or ";
			}
			HTML+=")";
		}
		HTML+= afterBlank;
		
		
		HTML+= "</div>";
		return HTML;
	}
}
