package quiz;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class ChallengeQuizInfo {
	private String challenger = null;
	private long timeTaken;
	private int numQuestionsCorrect;
	private int numQuestions;
	private Set<Integer> whichQuestionsCorrect;
	
	public ChallengeQuizInfo(long timeTaken, int numQuestionsCorrect, int numQuestions, String whichQuestionsCorrectString){
		this.timeTaken = timeTaken;
		this.numQuestionsCorrect = numQuestionsCorrect;
		this.numQuestions = numQuestions;
		String [] questionsCorrectStringArray = whichQuestionsCorrectString.split(Question.DELIMITER);
		Integer [] questionsCorrectIntArray = new Integer[numQuestionsCorrect];
		for(int i=0; i<numQuestionsCorrect; i++){
			questionsCorrectIntArray[i] = Integer.parseInt(questionsCorrectStringArray[i]);
		}
		whichQuestionsCorrect = new HashSet<Integer>(Arrays.asList(questionsCorrectIntArray));
	}
	
	public void setChallenger(String challenger){
		this.challenger = challenger;
	}
	
	public String getChallenger(){
		return challenger;
	}
	
	public long getTimeTaken(){
		return timeTaken;
	}
	
	public int getNumQuestionsCorrect(){
		return numQuestionsCorrect;
	}
	
	public int getNumQuestions(){
		return numQuestions;
	}
	
	public Set<Integer> whichQuestionsCorrect(){
		return whichQuestionsCorrect;
	}
	
	public String getHTML(){
		String HTML = "";
		HTML += "<h3 class='right plain'>" + challenger + " took: ";
		HTML += (double)(timeTaken)/1000 + " seconds and got ";
		HTML += "<br> and got" + numQuestionsCorrect + " out of " + numQuestions + " questions correct";
		HTML += "</h3>";
		return HTML;
	}
	
	public String getHTMLCentered(){
		String HTML = "";
		HTML += "<h3>" + challenger + " took: ";
		HTML += (double)(timeTaken)/1000 + " seconds and got ";
		HTML += "<br> and got " + numQuestionsCorrect + " out of " + numQuestions + " questions correct";
		HTML += "</h3>";
		return HTML;
	}
	
	public boolean challengerGotQuestionCorrect(int questionID){
		return whichQuestionsCorrect.contains(questionID);
	}
}
