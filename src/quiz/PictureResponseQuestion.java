package quiz;

public class PictureResponseQuestion extends Question {

	private String imageSource;
	private String[] possibleAnswers;
	//I think that we should have an option to add accompanying text. I'd honestly be too confused to know what to do for these Qs
	private String accompanyingText;
	public PictureResponseQuestion(int id, int quizId) {
		super(id, quizId);
		type = QUESTION_TYPE_PICTURE;
	}

	//Constructor for no accompanying text
	public PictureResponseQuestion(int id, int quizId, String imageSource, String answerString) {
		this(id, quizId, imageSource, answerString, "");
	}

	//Constructor with accompanying text
	public PictureResponseQuestion(int id, int quizId, String imageSource, String answerString, String extraText) {
		this(id, quizId);
		this.imageSource = imageSource;
		this.possibleAnswers = answerString.split(DELIMITER);
		this.accompanyingText = extraText;
	}


	//So.... Maybe we can do a default option? That'd be a good extension!
	//We could use a reg-ex to take into account common variances or extra info (maybe as long as they use the work 'blue' or something)
	@Override
	public boolean answerCorrect(Object answer) {
		String sAnswer = ((String) answer).toLowerCase().trim();
		for (int i = 0; i < possibleAnswers.length; i++) {
			if (sAnswer.equals(possibleAnswers[i].toLowerCase().trim())) return true;
		}
		return false;
	}

	@Override
	public Object getAnswers() {
		return possibleAnswers;
	}

	@Override
	public String getPrompt() {
		return imageSource;
	}

	@Override
	public String getHTML() {
		String HTML = "<div class='picture-response'>";
		HTML+= "<div class='question-text'>";
		HTML+= "<img alt='Failed to Load Image' src='" + imageSource + "' class='img-question'>";
		//		this.accompanyingText = "What is the above fruit?";

		//if any accompanying text
		if (!this.accompanyingText.equals("")){
			HTML+= "<br>";
			HTML+= this.accompanyingText;
		}
		HTML+= "</div>";
		HTML+= "<br>";
		//HTML+= "<form name='"+getId()+"'>";
		HTML+= " <input type='text' name='"+getId()+"' value=''> ";
		//HTML+= "</form>";
		HTML+= "</div>";
		return HTML;
	}

	@Override
	public String getCorrectedHTML(String userAnswer, boolean correct) {
		String HTML = "<div class='picture-response'>";
		HTML+= "<div class='question-text'>";
		HTML+= "<img alt='Failed to Load Image' src='" + imageSource + "'>";
		HTML+= "</div>";
		HTML+= "<br>";
		HTML+= " <input type='text' name='' value='"+userAnswer+"' readonly> (";
		if (correct){				
			HTML+= "<span style='background-color:"+Question.correctAnswerColor+"'>";
			HTML+= "CORRECT! :-D)";
			HTML+="</span>";
		} else {  
			int i = 0;
			for(String possibleAnswer : possibleAnswers){
				HTML+= "<span style='background-color:"+Question.correctedAnswerColor+"'>";
				HTML+= possibleAnswer;
				HTML+="</span>";
				if(++i!=possibleAnswers.length)HTML += " or ";
			}
			HTML+=")";
		}
		HTML+= "</div>";
		return HTML;
	}

}
