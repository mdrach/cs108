package quiz;


/* This class is used to hold basic info of the quiz.
 * An ArrayList of these is returned by DBConnection so that homepage.jsp
 * can form a list of quizzes for the user to choose from.
 */

public class QuizInfo {
	public String name;
	public String id;
	public String description;
	public String creator;
	public int categoryId;
	public int timesPlayed;
}
